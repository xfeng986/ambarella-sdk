/**
 * am_playback_new_if.h
 *
 * History:
 *  2015/07/24 - [Zhi He] create file
 *
 * Copyright (c) 2015 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __AM_PLAYBACK_NEW_IF_H__
#define __AM_PLAYBACK_NEW_IF_H__

//-----------------------------------------------------------------------
//
//  Error code
//
//-----------------------------------------------------------------------

typedef enum {
  EECode_OK = 0,

  EECode_NotInitilized = 0x001,
  EECode_NotRunning = 0x002,

  EECode_Error = 0x003,
  EECode_Closed = 0x004,
  EECode_Busy = 0x005,
  EECode_OSError = 0x006,
  EECode_IOError = 0x007,
  EECode_TimeOut = 0x008,
  EECode_TooMany = 0x009,
  EECode_OutOfCapability = 0x00a,

  EECode_NoMemory = 0x00c,
  EECode_NoPermission = 0x00d,
  EECode_NoImplement = 0x00e,
  EECode_NoInterface = 0x00f,

  EECode_NotExist = 0x010,
  EECode_NotSupported = 0x011,

  EECode_BadState = 0x012,
  EECode_BadParam = 0x013,
  EECode_BadCommand = 0x014,
  EECode_BadFormat = 0x015,
  EECode_BadMsg = 0x016,
  EECode_BadSessionNumber = 0x017,

  EECode_TryAgain = 0x018,
  EECode_DataCorruption = 0x019,
  EECode_DataMissing = 0x01a,

  EECode_InternalLogicalBug = 0x01c,
  EECode_ParseError = 0x021,

  EECode_UnknownError = 0x030,

  //not error, but a case, or need further API invoke
  EECode_OK_NoOutputYet = 0x43,
  EECode_OK_EOF = 0x45,

  EECode_RTSP_ServerNoSession = 0x80,
  EECode_RTSP_ServerNotSupportTCP = 0x81,
  EECode_RTSP_ServerNotSupportUDP = 0x82,
  EECode_RTSP_ServerError = 0x83,
} EECode;

extern const char *gfGetErrorCodeString(EECode code);

typedef unsigned int TGenericID;
typedef int TDimension;

//defines
#define DMaxPreferStringLen 16
#define DCorpLOGO "Ambarella"

#define DNonePerferString "AUTO"

//platform related
#define DNameTag_AMBA "AMBA"
#define DNameTag_ALSA "ALSA"
#define DNameTag_LinuxFB "LinuxFB"
#define DNameTag_LinuxUVC "LinuxUVC"

//private demuxer muxer
#define DNameTag_PrivateMP4 "PRMP4"
#define DNameTag_PrivateTS "PRTS"
#define DNameTag_PrivateRTSP "PRRTSP"

//external
#define DNameTag_FFMpeg "FFMpeg"
#define DNameTag_X264 "X264"
#define DNameTag_X265 "X265"
#define DNameTag_LIBAAC "LIBAAC"
#define DNameTag_HEVCHM "HEVCHM"

//injecters
#define DNameTag_AMBAEFM "AMBAEFM"

//-----------------------------------------------------------------------
//
//  Message and Command
//
//-----------------------------------------------------------------------

typedef enum {
  ECMDType_Invalid = 0x0000,

  ECMDType_FirstEnum = 0x0001,

  //common used
  ECMDType_Terminate = ECMDType_FirstEnum,
  ECMDType_StartRunning = 0x0002, //for AO, enter/exit OnRun
  ECMDType_ExitRunning = 0x0003,

  ECMDType_RunProcessing = 0x0004, //for processing, module specific work related, can not re-invoked start processing after exit processing
  ECMDType_ExitProcessing = 0x0005,

  ECMDType_Stop = 0x0010, //module start/stop, can be re-invoked
  ECMDType_Start = 0x0011,

  ECMDType_Pause = 0x0012,
  ECMDType_Resume = 0x0013,
  ECMDType_ResumeFlush = 0x0014,
  ECMDType_Flush = 0x0015,
  ECMDType_FlowControl = 0x0016,
  ECMDType_Suspend = 0x0017,
  ECMDType_ResumeSuspend = 0x0018,

  ECMDType_SwitchInput = 0x0019,

  ECMDType_SendData = 0x001a,
  ECMDType_Seek = 0x001b,

  ECMDType_Step = 0x0020,
  ECMDType_DebugDump = 0x0021,
  ECMDType_PrintCurrentStatus = 0x0022,
  ECMDType_SetRuntimeLogConfig = 0x0023,
  ECMDType_PlayUrl = 0x0024,
  ECMDType_PlayNewUrl = 0x0025,

  ECMDType_PurgeChannel = 0x0028,
  ECMDType_ResumeChannel = 0x0029,
  ECMDType_NavigationSeek = 0x002a,

  ECMDType_AddContent = 0x0030,
  ECMDType_RemoveContent = 0x0031,
  ECMDType_AddClient = 0x0032,
  ECMDType_RemoveClient = 0x0033,
  ECMDType_UpdateUrl = 0x0034,
  ECMDType_RemoveClientSession = 0x0035,
  ECMDType_UpdateRecSavingStrategy = 0x0036,

  //specific to each case
  ECMDType_ForceLowDelay = 0x0040,
  ECMDType_Speedup = 0x0041,
  ECMDType_DeleteFile = 0x0042,
  ECMDType_UpdatePlaybackSpeed = 0x0043,
  ECMDType_UpdatePlaybackLoopMode = 0x0044,
  ECMDType_DecoderZoom = 0x0045,

  ECMDType_EnableVideo = 0x0046,
  ECMDType_EnableAudio = 0x0047,

  ECMDType_NotifySynced = 0x0050,
  ECMDType_NotifySourceFilterBlocked = 0x0051,
  ECMDType_NotifyUDECInRuningState = 0x0052,
  ECMDType_NotifyBufferRelease = 0x0053,

  ECMDType_MuteAudio = 0x0060,
  ECMDType_UnMuteAudio = 0x0061,
  ECMDType_ConfigAudio = 0x0062,

  ECMDType_NotifyPrecacheDone = 0x0070,

  ECMDType_LastEnum = 0x0400,

} ECMDType;

typedef enum {
  EMSGType_Invalid = 0,

  EMSGType_FirstEnum = ECMDType_LastEnum,

  //common used
  EMSGType_ExternalMSG = EMSGType_FirstEnum,
  EMSGType_Timeout = 0x0401,
  EMSGType_InternalBug = 0x0402,
  EMSGType_RePlay = 0x0404,

  EMSGType_DebugDump_Flow = 0x0405,
  EMSGType_UnknownError = 0x406,
  EMSGType_MissionComplete = 0x407,
  EMSGType_TimeNotify = 0x408,
  EMSGType_DataNotify = 0x409,
  EMSGType_ControlNotify = 0x40a,
  EMSGType_ClientReconnectDone = 0x40b,

  EMSGType_StorageError = 0x0410,
  EMSGType_IOError = 0x0411,
  EMSGType_SystemError = 0x0412,
  EMSGType_DeviceError = 0x0413,

  EMSGType_StreamingError_TCPSocketConnectionClose = 0x0414,
  EMSGType_StreamingError_UDPSocketInvalidArgument = 0x0415,

  EMSGType_NetworkError = 0x0416,
  EMSGType_ServerError = 0x0417,

  EMSGType_DriverErrorBusy = 0x0420,
  EMSGType_DriverErrorNotSupport = 0x0421,
  EMSGType_DriverErrorOutOfCapability = 0x0422,
  EMSGType_DriverErrorNoPermmition = 0x0423,

  EMSGType_PlaybackEndNotify = 0x0428,
  EMSGType_AudioPrecacheReadyNotify = 0x0429,

  //for each user cases
  EMSGType_PlaybackEOS = 0x0430,
  EMSGType_RecordingEOS = 0x0431,
  EMSGType_NotifyNewFileGenerated = 0x0432,
  EMSGType_RecordingReachPresetDuration = 0x0433,
  EMSGType_RecordingReachPresetFilesize = 0x0434,
  EMSGType_RecordingReachPresetTotalFileNumbers = 0x0435,
  EMSGType_PlaybackStatisticsFPS = 0x0436,

  EMSGType_NotifyThumbnailFileGenerated = 0x0440,
  EMSGType_NotifyUDECStateChanges = 0x0441,
  EMSGType_NotifyUDECUpdateResolution = 0x0442,

  EMSGType_OpenSourceFail = 0x0450,
  EMSGType_OpenSourceDone = 0x0451,

  //application related
  EMSGType_WindowClose = 0x0460,
  EMSGType_WindowActive = 0x0461,
  EMSGType_WindowSize = 0x0462,
  EMSGType_WindowSizing = 0x0463,
  EMSGType_WindowMove = 0x0464,
  EMSGType_WindowMoving = 0x0465,

  EMSGType_WindowKeyPress = 0x0466,
  EMSGType_WindowKeyRelease = 0x0467,
  EMSGType_WindowLeftClick = 0x0468,
  EMSGType_WindowRightClick = 0x0469,
  EMSGType_WindowDoubleClick = 0x046a,
  EMSGType_WindowLeftRelease = 0x046b,
  EMSGType_WindowRightRelease = 0x046c,


  EMSGType_VideoSize = 0x0480,

  EMSGType_PostAgentMsg = 0x0490,

  EMSGType_ApplicationExit = 0x0500,

  //shared network related
  EMSGType_SayHelloDoneNotify = 0x0501,
  EMSGType_SayHelloFailNotify = 0x0502,
  EMSGType_SayHelloAlreadyInNotify = 0x503,

  EMSGType_JoinNetworkDoneNotify = 0x0504,
  EMSGType_JoinNetworkFailNotify = 0x0505,
  EMSGType_JoinNetworkAlreadyInNotify = 0x506,

  EMSGType_HandshakeDoneNotify = 0x0507,
  EMSGType_HandshakeFailNotify = 0x0508,

  EMSGType_SendFileAcceptNotify = 0x050a,
  EMSGType_SendFileDenyNotify = 0x050b,

  EMSGType_RequestDSSAcceptNotify = 0x050d,
  EMSGType_RequestDSSDenyNotify = 0x050e,

  EMSGType_QueryFriendListDoneNotify = 0x0512,
  EMSGType_QueryFriendListFailNotify = 0x0513,

  EMSGType_RequestDSS = 0x0540,
  EMSGType_RequestSendFile = 0x0541,
  EMSGType_RequestRecieveFile = 0x0542,

  EMSGType_HostDepartureNotify = 0x0550,
  EMSGType_FriendOnlineNotify = 0x0551,
  EMSGType_FriendOfflineNotify = 0x0552,
  EMSGType_FriendNotReachableNotify = 0x0553,
  EMSGType_FriendNotReachableNotifyFromHost = 0x0554,
  EMSGType_FriendInvitationNotify = 0x0555,

  EMSGType_HostIsNotReachableNotify = 0x0556,

  EMSGType_UserConfirmPermitDSS = 0x580,
  EMSGType_UserConfirmDenyDSS = 0x581,

  EMSGType_UserConfirmPermitRecieveFile = 0x0582,
  EMSGType_UserConfirmDenyRecieveFile = 0x0583,

  EMSGType_RecieveFileFinishNotify = 0x0584,
  EMSGType_SendFileFinishNotify = 0x0585,
  EMSGType_RecieveFileAbortNotify = 0x0586,

  EMSGType_DSViewEndNotify = 0x0587,
  EMSGType_DSShareAbortNotify = 0x0588,

} EMSGType;

typedef struct {
  unsigned int code;
  void    *pExtra;
  unsigned short sessionID;
  unsigned char flag;
  unsigned char needFreePExtra;

  unsigned short owner_index;
  unsigned char owner_type;
  unsigned char identifyer_count;

  unsigned long p_owner, owner_id;
  unsigned long p_agent_pointer;

  unsigned long p0, p1, p2, p3, p4;
  float f0;
} SMSG;

enum StreamType {
  StreamType_Invalid = 0,
  StreamType_Video,
  StreamType_Audio,
  StreamType_Subtitle,
  StreamType_PrivateData,

  StreamType_Cmd,

  StreamType_TotalNum,
};

typedef enum {
  ENavigationPosition_Invalid = 0,
  ENavigationPosition_Begining,
  ENavigationPosition_End,
  ENavigationPosition_LastKeyFrame,
} ENavigationPosition;

typedef enum {
  EPixelFMT_invalid = 0x00,
  EPixelFMT_rgba8888 = 0x01,
  EPixelFMT_yuv420p = 0x02,
  EPixelFMT_yuv422p = 0x03,
  EPixelFMT_yuv420_nv12 = 0x04,
  EPixelFMT_agbr8888 = 0x05,
  EPixelFMT_ayuv8888 = 0x06,
  EPixelFMT_rgb565 = 0x07,
  EPixelFMT_vyu565 = 0x08,
} EPixelFMT;

enum DecoderFeedingRule {
  DecoderFeedingRule_NotValid = 0,
  DecoderFeedingRule_AllFrames,
  DecoderFeedingRule_RefOnly,
  DecoderFeedingRule_IOnly,
  DecoderFeedingRule_IDROnly,
};

typedef struct {
  unsigned int check_field;

  unsigned char b_digital_vout;
  unsigned char b_hdmi_vout;
  unsigned char b_cvbs_vout;
  unsigned char reserved0;
} SConfigVout;

typedef struct {
  unsigned int check_field;
  char audio_device_name[32];
} SConfigAudioDevice;

typedef struct {
  unsigned char check_field;
  TGenericID component_id;

  long long target;//ms
  ENavigationPosition position;
} SPbSeek;

typedef struct {
  unsigned int check_field;
  TGenericID component_id;

  unsigned char direction;
  unsigned char feeding_rule;
  unsigned short speed;
} SPbFeedingRule;

typedef struct {
  unsigned int check_field;
  TGenericID component_id;
} SResume1xPlayback;

typedef struct {
  unsigned int check_field;
  TGenericID component_id;
  long long time;
} SQueryLastShownTimeStamp;

typedef enum {

  //pre running, persist config
  EGenericEngineConfigure_RTSPServerPort = 0x0010,
  EGenericEngineConfigure_RTSPServerNonblock = 0x0011,
  EGenericEngineConfigure_VideoStreamID = 0x0012,
  EGenericEngineConfigure_ConfigVout = 0x0013,
  EGenericEngineConfigure_ConfigAudioDevice = 0x0014,
  EGenericEngineConfigure_EnableFastFWFastBWBackwardPlayback = 0x0015,
  EGenericEngineConfigure_DisableFastFWFastBWBackwardPlayback = 0x0016,
  EGenericEngineConfigure_EnableDumpBitstream = 0x0017,
  EGenericEngineConfigure_DumpBitstreamOnly = 0x0018,

  EGenericEngineConfigure_FastForward = 0x0020,
  EGenericEngineConfigure_FastBackward = 0x0021,
  EGenericEngineConfigure_StepPlay = 0x0022,
  EGenericEngineConfigure_Pause = 0x0023,
  EGenericEngineConfigure_Resume = 0x0024,
  EGenericEngineConfigure_Seek = 0x0025,
  EGenericEngineConfigure_LoopMode = 0x0026,
  EGenericEngineConfigure_FastForwardFromBegin = 0x0027,
  EGenericEngineConfigure_FastBackwardFromEnd = 0x0028,
  EGenericEngineConfigure_Resume1xFromBegin = 0x0029,
  EGenericEngineConfigure_Resume1xFromCurrent = 0x002a,
  EGenericEngineConfigure_QueryLastDisplayTimestamp = 0x002b,

  //video capture
  EGenericEngineConfigure_VideoCapture_Params = 0x0100,

  //audio playback
  EGenericEngineConfigure_AudioPlayback_SelectAudioSource = 0x0200,
  EGenericEngineConfigure_AudioPlayback_Mute,
  EGenericEngineConfigure_AudioPlayback_UnMute,
  EGenericEngineConfigure_AudioPlayback_SetVolume,

  //audio capture
  EGenericEngineConfigure_AudioCapture_Mute = 0x0210,
  EGenericEngineConfigure_AudioCapture_UnMute = 0x0211,

  //streaming related
  EGenericEngineConfigure_ReConnectServer = 0x0220,
  EGenericEngineConfigure_RTSPClientTryTCPModeFirst = 0x0221,

  //video encoder
  EGenericEngineConfigure_VideoEncoder_Params = 0x0400,
  EGenericEngineConfigure_VideoEncoder_Bitrate,
  EGenericEngineConfigure_VideoEncoder_Framerate,
  EGenericEngineConfigure_VideoEncoder_DemandIDR,
  EGenericEngineConfigure_VideoEncoder_GOP,

  //record related, for pipeline
  EGenericEngineConfigure_Record_Saving_Strategy = 0x0450,
  EGenericEngineConfigure_Record_SetSpecifiedInformation = 0x0451,
  EGenericEngineConfigure_Record_GetSpecifiedInformation = 0x0452,
  EGenericEngineConfigure_Record_Suspend = 0x0453,
  EGenericEngineConfigure_Record_ResumeSuspend = 0x0454,
} EGenericEngineConfigure;

typedef struct {
  unsigned int check_field;
  unsigned int port;
} SConfigRTSPServerPort;

typedef struct {
  unsigned int check_field;
  unsigned int stream_id;
} SConfigVideoStreamID;

typedef struct {
  unsigned int check_field;
  TGenericID component_id;
} SUserParamStepPlay;

typedef struct {
  unsigned int check_field;
  TGenericID component_id;
} SUserParamPause;

typedef struct {
  unsigned int check_field;
  TGenericID component_id;
} SUserParamResume;

#define DMAX_PATHFILE_NANE_LENGTH 8192
#define DMAX_FILE_EXTERTION_NANE_LENGTH 32

//-----------------------------------------------------------------------
//
//  Media Engine
//
//-----------------------------------------------------------------------

#define DCAL_BITMASK(x) (1 << x)

#define DCOMPONENT_TYPE_IN_GID 24
#define DCOMPONENT_VALID_MARKER_BITS 23
#define DCOMPONENT_INDEX_MASK_IN_GID 0xffff

#define DCOMPONENT_TYPE_FROM_GENERIC_ID(x) (x >> DCOMPONENT_TYPE_IN_GID)
#define DCOMPONENT_INDEX_FROM_GENERIC_ID(x) (x & DCOMPONENT_INDEX_MASK_IN_GID)
#define DGENERIC_ID_FROM_COMPONENT_TYPE_INDEX(t, i) ((t << DCOMPONENT_TYPE_IN_GID) | (i & DCOMPONENT_INDEX_MASK_IN_GID) | (1 << DCOMPONENT_VALID_MARKER_BITS))
#define IS_VALID_COMPONENT_ID(x) (x & DCAL_BITMASK(DCOMPONENT_VALID_MARKER_BITS))

enum {
  EGenericPipelineState_not_inited = 0x0,
  EGenericPipelineState_build_gragh = 0x1,
  EGenericPipelineState_running = 0x3,
  EGenericPipelineState_suspended = 0x4,
};

enum {
  EGenericComponentType_TAG_filter_start = 0x00,
  EGenericComponentType_Demuxer = EGenericComponentType_TAG_filter_start,
  EGenericComponentType_VideoDecoder = 0x01,
  EGenericComponentType_AudioDecoder = 0x02,
  EGenericComponentType_VideoEncoder = 0x03,
  EGenericComponentType_AudioEncoder = 0x04,
  EGenericComponentType_VideoRenderer = 0x05,
  EGenericComponentType_AudioRenderer = 0x06,
  EGenericComponentType_Muxer = 0x07,
  EGenericComponentType_StreamingTransmiter = 0x08,
  EGenericComponentType_FlowController = 0x09,
  EGenericComponentType_VideoCapture = 0x0a,
  EGenericComponentType_AudioCapture = 0x0b,
  EGenericComponentType_VideoInjecter = 0x0c,

  EGenericComponentType_TAG_filter_end,

  EGenericPipelineType_Playback = 0x70,
  EGenericPipelineType_Recording = 0x71,
  EGenericPipelineType_Streaming = 0x72,

  EGenericComponentType_TAG_proxy_start = 0x80,
  EGenericComponentType_StreamingServer = EGenericComponentType_TAG_proxy_start,
  EGenericComponentType_StreamingContent = 0x81,
  EGenericComponentType_ConnectionPin = 0x82,

  EGenericComponentType_TAG_proxy_end,
};

class IGenericEngineControl
{
public:
  virtual EECode print_current_status(TGenericID id = 0, unsigned int print_flag = 0) = 0;

public:
  virtual EECode begin_config_process_pipeline(unsigned int customized_pipeline = 1) = 0;
  virtual EECode finalize_config_process_pipeline() = 0;

  virtual EECode new_component(unsigned int component_type, TGenericID &component_id, const char *prefer_string = NULL) = 0;
  virtual EECode connect_component(TGenericID &connection_id, TGenericID upper_component_id, TGenericID down_component_id, StreamType pin_type) = 0;

  virtual EECode setup_playback_pipeline(TGenericID &playback_pipeline_id, TGenericID video_source_id, TGenericID audio_source_id, TGenericID video_decoder_id, TGenericID audio_decoder_id, TGenericID video_renderer_id, TGenericID audio_renderer_id) = 0;
  virtual EECode setup_recording_pipeline(TGenericID &recording_pipeline_id, TGenericID video_source_id, TGenericID audio_source_id, TGenericID sink_id) = 0;
  virtual EECode setup_streaming_pipeline(TGenericID &streaming_pipeline_id, TGenericID streaming_transmiter_id, TGenericID streaming_server_id, TGenericID video_source_id, TGenericID audio_source_id, TGenericID video_connection_id, TGenericID audio_connection_id) = 0;

public:
  virtual EECode generic_control(EGenericEngineConfigure config_type, void *param) = 0;

  //set urls
  virtual EECode set_source_url(TGenericID source_component_id, char *url) = 0;
  virtual EECode set_sink_url(TGenericID sink_component_id, char *url) = 0;
  virtual EECode set_streaming_url(TGenericID pipeline_id, char *url) = 0;

  virtual EECode set_app_msg_callback(void (*MsgProc)(void *, SMSG &), void *context) = 0;

  virtual EECode run_processing() = 0;
  virtual EECode exit_processing() = 0;

  virtual EECode start() = 0;
  virtual EECode stop() = 0;

public:
  virtual void destroy() = 0;

protected:
  virtual ~IGenericEngineControl() {}
};

extern IGenericEngineControl *CreateGenericMediaEngine();

extern unsigned int gfGetMaxEncodingStreamNumber();

typedef struct {
  unsigned int video_width;
  unsigned int video_height;

  unsigned int video_framerate_num;
  unsigned int video_framerate_den;

  unsigned int profile_indicator;
  unsigned int level_indicator;

  unsigned char audio_channel_number;
  unsigned char audio_sample_size; // bits
  unsigned char format;
  unsigned char video_format;

  unsigned int audio_sample_rate;
  unsigned int audio_frame_size;
} SMediaInfo;

typedef struct {
  unsigned char *p_buffer;
  unsigned int buffer_size;
  unsigned int data_size;
} SGenericDataBuffer;

typedef enum {
  EMediaProbeType_Invalid = 0x00,
  EMediaProbeType_MP4 = 0x01,
} EMediaProbeType;

typedef struct {
  SMediaInfo info;

  unsigned int tot_video_frames;
  unsigned int tot_audio_frames;

  unsigned char *p_video_extradata;
  unsigned int video_extradata_len;
} SMediaProbedInfo;

class IMediaProbe
{
public:
  virtual void Delete() = 0;

protected:
  virtual ~IMediaProbe() {}

public:
  virtual EECode Open(char *filename, SMediaProbedInfo *info) = 0;
  virtual EECode Close() = 0;

  virtual SGenericDataBuffer *GetKeyFrame(unsigned int key_frame_index) = 0;
  virtual SGenericDataBuffer *GetFrame(unsigned int frame_index) = 0;
  virtual void ReleaseFrame(SGenericDataBuffer *frame) = 0;
};

IMediaProbe *gfCreateMediaProbe(EMediaProbeType type);

typedef enum {
  EThumbPlayDisplayLayout_Invalid = 0x00,
  EThumbPlayDisplayLayout_Rectangle = 0x01,
} EThumbPlayDisplayLayout;

typedef enum {
  EDisplayDevice_Invalid = 0x00,
  EDisplayDevice_DIGITAL = 0x01,
  EDisplayDevice_HDMI = 0x02,
  EDisplayDevice_CVBS = 0x03,
} EDisplayDevice;

typedef struct {
  TDimension rows;
  TDimension columns;

  TDimension tot_width, tot_height;
  TDimension border_x, border_y;

  //derived parameters
  TDimension rect_width;
  TDimension rect_height;
} SThumbPlayDisplayRectangleLayoutParams;

typedef struct {
  unsigned char *p[4];
  unsigned int linesize[4];
  EPixelFMT fmt;
} SThumbPlayBufferDesc;

class IThumbPlayControl
{
public:
  virtual EECode SetMediaPath(char *url) = 0;
  virtual EECode SetDisplayDevice(EDisplayDevice device) = 0;
  virtual unsigned int GetTotalFileNumber() = 0;
  virtual EECode SetDisplayLayout(EThumbPlayDisplayLayout layout, void *params) = 0;
  virtual EECode ExitDeviceMode() = 0;
  virtual EECode SetDisplayBuffer(SThumbPlayBufferDesc *buffer, TDimension buffer_width, TDimension buffer_height) = 0;
  virtual EECode DisplayThumb2Device(unsigned int begin_index) = 0;
  virtual EECode DisplayThumb2Buffer(unsigned int begin_index) = 0;

public:
  virtual void Destroy() = 0;

protected:
  virtual ~IThumbPlayControl() {}

};

IThumbPlayControl *CreateThumbPlay();

#endif

