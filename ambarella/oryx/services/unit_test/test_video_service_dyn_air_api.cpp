/*
 * test_video_service_dyn_air_api.cpp
 *
 *  History:
 *    Nov 27, 2015 - [Shupeng Ren] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdio.h>
#include <stdint.h>
#include <getopt.h>
#include <signal.h>
#include <math.h>
#include <map>
#include <vector>
#include <string>

#include "am_base_include.h"
#include "am_log.h"
#include "am_define.h"

#include "am_api_helper.h"
#include "am_api_video.h"
#include "am_video_types.h"

using std::map;
using std::string;
using std::vector;

#define NO_ARG 0
#define HAS_ARG 1

#define MAX_STREAM_NUM 4

#define VERIFY_PARA_1(x, min) \
    do { \
      if ((x) < min) { \
        printf("Parameter wrong: %d\n", (x)); \
        return -1; \
      } \
    } while (0)

#define VERIFY_PARA_2(x, min, max) \
    do { \
      if (((x) < min) || ((x) > max)) { \
        printf("Parameter wrong: %d\n", (x)); \
        return -1; \
      } \
    } while (0)

#define VERIFY_PARA_1_FLOAT(x, min) \
    do { \
      if ((x) < min) { \
        printf("Parameter wrong: %f\n", (x)); \
        return -1; \
      } \
    } while (0)

#define VERIFY_PARA_2_FLOAT(x, min, max) \
    do { \
      if (((x) < min) || ((x) > max)) { \
        printf("Parameter wrong: %f\n", (x)); \
        return -1; \
      } \
    } while (0)

static AMAPIHelperPtr g_api_helper = NULL;
static bool show_status_flag = false;
static bool show_info_flag = false;
static bool show_warp_flag = false;
static bool show_dptz_ratio_flag = false;
static bool show_dptz_size_flag = false;
static bool set_dptz_ratio_flag = false;
static bool set_dptz_size_flag = false;
static bool show_lbr_flag = false;
static bool show_eis_flag = false;
static bool show_bitrate_flag = false;
static bool show_framefactor_flag = false;
static bool show_mjpeg_flag = false;
static bool show_h264_gop_flag = false;
static bool show_stream_offset_flag = false;
static bool pause_flag = false;
static bool resume_flag = false;
static bool restart_flag = false;
static bool save_overlay_flag = false;
static bool get_platform_stream_num_max_flag = false;
static bool get_platform_buffer_num_max_flag = false;
static bool get_overlay_max_flag = false;
static bool destroy_overlay_flag = false;
static uint32_t force_idr_id = 0;
AM_STREAM_ID current_stream_id = AM_STREAM_ID_INVALID;
AM_SOURCE_BUFFER_ID current_buffer_id = AM_SOURCE_BUFFER_INVALID;

static void sigstop(int32_t arg)
{
  INFO("signal comes!\n");
  exit(1);
}

struct setting_option {
    bool is_set;
    union {
        bool     v_bool;
        float    v_float;
        int32_t  v_int;
        uint32_t v_uint;
    } value;

    string str;
};

static setting_option vout_stop;

struct dptz_ratio_setting {
    bool is_set;
    setting_option pan;
    setting_option tilt;
    setting_option zoom;
};

struct dptz_size_setting {
    bool is_set;
    setting_option x;
    setting_option y;
    setting_option w;
    setting_option h;
};

struct warp_setting {
    bool is_set;
    setting_option ldc_strength;
    setting_option pano_hfov_degree;
    setting_option warp_region_yaw;
};

#define SOURCE_BUFFER_MAX_NUM 4

struct overlay_data_setting {
    bool is_set;
    setting_option w;
    setting_option h;
    setting_option x;
    setting_option y;
    setting_option type;
    setting_option str;
    setting_option pre_str;
    setting_option suf_str;
    setting_option spacing;
    setting_option en_msec;
    setting_option bg_color;
    setting_option font_name;
    setting_option font_size;
    setting_option font_color;
    setting_option font_outline_w;
    setting_option font_outline_color;
    setting_option font_hor_bold;
    setting_option font_ver_bold;
    setting_option font_italic;
    setting_option bmp;
    setting_option color_key;
    setting_option color_range;
    setting_option bmp_num;
    setting_option interval;
};

struct overlay_attr_setting {
    bool is_set;
    setting_option  x;
    setting_option  y;
    setting_option  w;
    setting_option  h;
    setting_option  rotate;
    setting_option  buf_num;
    setting_option  bg_color;
};

struct overlay_area_setting {
    bool is_set;
    int32_t area_id;
    int32_t data_idx;
    overlay_attr_setting attr;
    overlay_data_setting data;
};

struct overlay_setting {
    bool                  is_set;
    overlay_area_setting  init;
    overlay_area_setting  add_data;
    overlay_area_setting  update_data;
    overlay_area_setting  remove_data;
    overlay_area_setting  remove;
    overlay_area_setting  enable;
    overlay_area_setting  disable;
    overlay_area_setting  show;
};

struct lbr_setting {
    bool is_set;
    setting_option lbr_enable;
    setting_option lbr_auto_bitrate_target;
    setting_option lbr_bitrate_ceiling;
    setting_option lbr_frame_drop;
};

struct eis_setting {
    bool is_set;
    setting_option eis_mode;
};

struct bitrate_setting {
    bool is_set;
    setting_option target_bitrate;
};

struct framefactor_setting {
    bool is_set;
    setting_option mul;
    setting_option div;
};

struct mjpeg_setting {
    bool is_set;
    setting_option quality;
};

struct h264_gop_setting {
    bool is_set;
    setting_option N;
    setting_option idr;
};

struct stream_offset_setting {
    bool is_set;
    setting_option x;
    setting_option y;
};

typedef map<AM_STREAM_ID, overlay_setting> overlay_setting_map;
static overlay_setting_map g_overlay_setting;
static lbr_setting g_lbr_cfg[MAX_STREAM_NUM];
static warp_setting g_warp_cfg;
typedef map<AM_SOURCE_BUFFER_ID, dptz_ratio_setting> dptz_ratio_setting_map;
static dptz_ratio_setting_map g_dptz_ratio_cfg;
typedef map<AM_SOURCE_BUFFER_ID, dptz_size_setting> dptz_size_setting_map;
static dptz_size_setting_map g_dptz_size_cfg;
static eis_setting g_eis_cfg;
static bitrate_setting g_bitrate_cfg;
static framefactor_setting g_framefactor_cfg;
static mjpeg_setting g_mjpeg_cfg;
static h264_gop_setting g_h264_gop_cfg;
static stream_offset_setting g_stream_offset_cfg;

enum numeric_short_options {
  NONE = 0,
  H264 = 1,
  MJPEG = 2,

  OFF = 0,
  ENCODE = 1,
  PREVIEW = 2,

  CHROMA_420 = 0,
  CHROMA_422 = 1,
  CHROMA_MONO = 2,

  NEMERIC_BEGIN = 'z',

  STREAM_NUM_MAX,
  BUFFER_NUM_MAX,

  //dynamic control
  VOUT_HALT,

  FORCE_IDR,

  //DPTZ Warp config
  LDC_STRENGTH,
  PANO_HFOV_DEGREE,
  WARP_REGION_YAW,
  PAN_RATIO,
  TILT_RATIO,
  ZOOM_RATIO,
  DPTZ_X,
  DPTZ_Y,
  DPTZ_W,
  DPTZ_H,

  //Overlay config
  OVERLAY_MAX,
  OVERLAY_INIT,
  OVERLAY_DATA_ADD,
  OVERLAY_DATA_UPDATE,
  OVERLAY_DATA_REMOVE,
  OVERLAY_REMOVE,
  OVERLAY_ENABLE,
  OVERLAY_DISABLE,
  OVERLAY_SHOW,
  OVERLAY_SAVE,
  OVERLAY_DESTROY,
  OVERLAY_AREA_BG_COLOR,
  OVERLAY_AREA_BUF_NUM,
  OVERLAY_AREA_ROTATE,
  OVERLAY_W,
  OVERLAY_H,
  OVERLAY_X,
  OVERLAY_Y,
  OVERLAY_DATA_INDEX,
  OVERLAY_DATA_TYPE,
  OVERLAY_TEXT_STRING,
  OVERLAY_TEXT_PREFIX_STRING,
  OVERLAY_TEXT_SUFFIX_STRING,
  OVERLAY_TEXT_MSEC_EN,
  OVERLAY_TEXT_BG_COLOR,
  OVERLAY_TEXT_SPACING,
  OVERLAY_FONT_TYPE,
  OVERLAY_FONT_SIZE,
  OVERLAY_FONT_COLOR,
  OVERLAY_FONT_OUTLINE_WIDTH,
  OVERLAY_FONT_OUTLINE_COLOR,
  OVERLAY_FONT_HOR_BOLD,
  OVERLAY_FONT_VER_BOLD,
  OVERLAY_FONT_ITALIC,
  OVERLAY_BMP,
  OVERLAY_BMP_NUM,
  OVERLAY_BMP_INTERVAL,
  OVERLAY_COLOR_KEY,
  OVERLAY_COLOR_RANGE,

  //LBR
  LBR_ENABLE,
  LBR_AUTO_BITRATE_TARGET,
  LBR_BITRATE_CEILING,
  LBR_FRAME_DROP,

  //EIS
  EIS_MODE,

  //stream bitrate/frame factor
  BITRATE_GET,
  BITRATE_SET,
  FRAMEFACTOR_GET,
  FRAMEFACTOR_SET,
  MJPEG_QUALITY_GET,
  MJPEG_QUALITY_SET,
  H264_GOP_GET,
  H264_GOP_N_SET,
  H264_GOP_IDR_SET,
  STREAM_OFFSET_GET,
  STREAM_OFFSET_X,
  STREAM_OFFSET_Y,

  STOP_ENCODING,
  START_ENCODING,

  APPLY,

  SHOW_STREAM_STATUS,
  SHOW_WARP_CFG,
  SHOW_DPTZ_RATIO_CFG,
  SHOW_DPTZ_SIZE_CFG,
  SHOW_LBR_CFG,
  SHOW_EIS_CFG,
  SHOW_ALL_INFO
};

static struct option long_options[] =
{
 {"help",               NO_ARG,   0,  'h'},

 {"buffer_max_n",       NO_ARG,   0,  BUFFER_NUM_MAX},
 {"buffer",             HAS_ARG,  0,  'b'},
 {"stream_max_n",       NO_ARG,   0,  STREAM_NUM_MAX},
 {"stream",             HAS_ARG,  0,  's'},

 {"vout-halt",          HAS_ARG,  0,  VOUT_HALT},
 {"force-idr",          NO_ARG,   0,  FORCE_IDR},

 {"ldc-strength",       HAS_ARG,  0,  LDC_STRENGTH},
 {"pano-hfov-degree",   HAS_ARG,  0,  PANO_HFOV_DEGREE},
 {"warp-region-yaw",    HAS_ARG,  0,  WARP_REGION_YAW},
 {"pan-ratio",          HAS_ARG,  0,  PAN_RATIO},
 {"tilt-ratio",         HAS_ARG,  0,  TILT_RATIO},
 {"zoom-ratio",         HAS_ARG,  0,  ZOOM_RATIO},
 {"dptz-x",             HAS_ARG,  0,  DPTZ_X},
 {"dptz-y",             HAS_ARG,  0,  DPTZ_Y},
 {"dptz-w",             HAS_ARG,  0,  DPTZ_W},
 {"dptz-h",             HAS_ARG,  0,  DPTZ_H},

 //below is overlay option
 //get platform max area number for overlay
 {"overlay-max",        NO_ARG,  0, OVERLAY_MAX},
 {"overlay-init",       NO_ARG,  0, OVERLAY_INIT}, //init a overlay area
 {"overlay-data-add",   HAS_ARG, 0, OVERLAY_DATA_ADD}, //add a data to area
 {"overlay-data-update",HAS_ARG, 0, OVERLAY_DATA_UPDATE}, //update a area data
 {"overlay-data-remove",HAS_ARG, 0, OVERLAY_DATA_REMOVE}, //remove a area data
 {"overlay-remove",     HAS_ARG, 0, OVERLAY_REMOVE}, //remove a area
 {"overlay-enable",     HAS_ARG, 0, OVERLAY_ENABLE}, //enable a area
 {"overlay-disable",    HAS_ARG, 0, OVERLAY_DISABLE}, //disable a area
 {"overlay-show",       HAS_ARG, 0, OVERLAY_SHOW}, //show a area parameters
 {"overlay-save",       NO_ARG,  0, OVERLAY_SAVE}, //save user setting
 {"overlay-destroy",    NO_ARG,  0, OVERLAY_DESTROY}, //destory all overlay
 {"overlay-w",          HAS_ARG, 0, OVERLAY_W}, //area or data block width
 {"overlay-h",          HAS_ARG, 0, OVERLAY_H}, //area or data block height
 {"overlay-x",          HAS_ARG, 0, OVERLAY_X}, //area or data block offset x
 {"overlay-y",          HAS_ARG, 0, OVERLAY_Y}, //area or data block offset y
 {"overlay-area-buf-n", HAS_ARG, 0, OVERLAY_AREA_BUF_NUM}, //area background color
 {"overlay-area-bg-c",  HAS_ARG, 0, OVERLAY_AREA_BG_COLOR}, //area background color
 {"overlay-area-rotate",HAS_ARG, 0, OVERLAY_AREA_ROTATE}, //rotate
 {"overlay-data-type",  HAS_ARG, 0, OVERLAY_DATA_TYPE}, //data block type
 {"overlay-data-idx",   HAS_ARG, 0, OVERLAY_DATA_INDEX}, //data block index in area
 {"overlay-text-str",   HAS_ARG, 0, OVERLAY_TEXT_STRING}, //string
 {"overlay-text-prestr",HAS_ARG, 0, OVERLAY_TEXT_PREFIX_STRING}, //prefix string add to timestamp
 {"overlay-text-sufstr",HAS_ARG, 0, OVERLAY_TEXT_SUFFIX_STRING}, //suffix string add to timestamp
 {"overlay-text-spaing",HAS_ARG, 0, OVERLAY_TEXT_SPACING}, //spacing for text type
 {"overlay-text-bg-c",  HAS_ARG, 0, OVERLAY_TEXT_BG_COLOR}, //text type background color
 {"overlay-text-msec",  HAS_ARG, 0, OVERLAY_TEXT_MSEC_EN}, //text type outline color
 {"overlay-font-t",     HAS_ARG, 0, OVERLAY_FONT_TYPE}, //font type name
 {"overlay-font-s",     HAS_ARG, 0, OVERLAY_FONT_SIZE}, //font size
 {"overlay-font-c",     HAS_ARG, 0, OVERLAY_FONT_COLOR}, //font color
 {"overlay-font-ol-w",  HAS_ARG, 0, OVERLAY_FONT_OUTLINE_WIDTH}, //font outline width
 {"overlay-font-ol-c",  HAS_ARG, 0, OVERLAY_FONT_OUTLINE_COLOR}, //text type outline color
 {"overlay-font-hb",    HAS_ARG, 0, OVERLAY_FONT_HOR_BOLD}, //font hor_bold
 {"overlay-font-vb",    HAS_ARG, 0, OVERLAY_FONT_VER_BOLD}, //font ver_bold
 {"overlay-font-i",     HAS_ARG, 0, OVERLAY_FONT_ITALIC}, //font italic
 {"overlay-bmp",        HAS_ARG, 0, OVERLAY_BMP}, //bmp file path
 {"overlay-bmp-num",    HAS_ARG, 0, OVERLAY_BMP_NUM}, //bmp number in the animation file
 {"overlay-bmp-interval",HAS_ARG, 0,OVERLAY_BMP_INTERVAL}, //frame interval to display animation
 {"overlay-color-k",    HAS_ARG, 0, OVERLAY_COLOR_KEY}, //color to transparent
 {"overlay-color-r",    HAS_ARG, 0, OVERLAY_COLOR_RANGE}, //color range to transparent

 {"lbr-en",             HAS_ARG,  0,  LBR_ENABLE},
 {"lbr-abt",            HAS_ARG,  0,  LBR_AUTO_BITRATE_TARGET},
 {"lbr-bc",             HAS_ARG,  0,  LBR_BITRATE_CEILING},
 {"lbr-fd",             HAS_ARG,  0,  LBR_FRAME_DROP},

 {"eis-mode",           HAS_ARG,  0,  EIS_MODE},

 {"bitrate-get",        NO_ARG,   0,  BITRATE_GET},
 {"bitrate-set",        HAS_ARG,  0,  BITRATE_SET},
 {"framefactor-get",    NO_ARG,   0,  FRAMEFACTOR_GET},
 {"framefactor-set",    HAS_ARG,  0,  FRAMEFACTOR_SET},
 {"mjpeg-quality-get",  NO_ARG,   0,  MJPEG_QUALITY_GET},
 {"mjpeg-quality-set",  HAS_ARG,  0,  MJPEG_QUALITY_SET},
 {"h264-gop-get",       NO_ARG,   0,  H264_GOP_GET},
 {"h264-gop-n-set",     HAS_ARG,  0,  H264_GOP_N_SET},
 {"h264-gop-idr-set",   HAS_ARG,  0,  H264_GOP_IDR_SET},
 {"stream-offset-get",  NO_ARG,   0,  STREAM_OFFSET_GET},
 {"stream-offset-x",    HAS_ARG,  0,  STREAM_OFFSET_X},
 {"stream-offset-y",    HAS_ARG,  0,  STREAM_OFFSET_Y},

 {"pause",              NO_ARG,   0,  STOP_ENCODING},
 {"resume",             NO_ARG,   0,  START_ENCODING},

 {"restart",            NO_ARG,   0,  APPLY},

 {"show-stream-status", NO_ARG,   0,  SHOW_STREAM_STATUS},
 {"show-warp-cfg",      NO_ARG,   0,  SHOW_WARP_CFG},
 {"show-dptz-ratio",    NO_ARG,   0,  SHOW_DPTZ_RATIO_CFG},
 {"show-dptz-size",     NO_ARG,   0,  SHOW_DPTZ_SIZE_CFG},
 {"show-lbr-cfg",       NO_ARG,   0,  SHOW_LBR_CFG},
 {"show-eis-cfg",       NO_ARG,   0,  SHOW_EIS_CFG},
 {"show-all-info",      NO_ARG,   0,  SHOW_ALL_INFO},
 {0, 0, 0, 0}
};

static const char *short_options = "hb:s:";

struct hint32_t_s {
    const char *arg;
    const char *str;
};

static const hint32_t_s hint32_t[] =
{
 {"",     "\t\t\t" "Show usage\n"},

 {"",     "\t\t"   "Show platform support max buffer number"},
 {"0-3",  "\t\t"   "Source buffer ID. 0: Main buffer; 1: 2nd buffer; "
                   "2: 3rd buffer; 3: 4th buffer. Set to config file."},
 {"",     "\t\t"   "Show platform support max stream number"},
 {"0-3",  "\t\t"   "Stream ID"},

 {"0|1",  "\t\t"   "shutdown vout device,0:VOUTB,1:VOUTA"},

 {"",     "\t\t\t" "force IDR at once for current stream\n"},

 {"0.0-20.0", "\t" "LDC strength"},
 {"1.0-180.0", ""  "Panorama HFOV degree"},
 {"-90-90",   "\t" "Lens Yaw in degree"},
 {"-1.0-1.0", "\t" "pan ratio"},
 {"-1.0-1.0", "\t" "tilt ratio"},
 {"0.1-8.0", "\t" "zoom ratio\n"
                   "\t\t\t\t\t0.1 zoom out to FOV\n"
                   "\t\t\t\t\tDPTZ-I  1st buffer scalability 1/4X  - 8X\n"
                   "\t\t\t\t\tDPTZ-II 2nd buffer scalability 1/16X - 1X\n"
                   "\t\t\t\t\tDPTZ-II 3rd buffer scalability 1/8X  - 8X\n"
                   "\t\t\t\t\tDPTZ-II 4th buffer scalability 1/16X - 8X\n"},
 {"",     "\t\t\t" "dptz offset.x"},
 {"",     "\t\t\t" "dptz offset.y"},
 {"",     "\t\t\t" "dptz width"},
 {"",     "\t\t\t" "dptz height"},

 //overlay
 {"",     "\t\t"   "get the platform support max overlay area number"},
 {"",     "\t\t"   "init a area for overlay, the return value is the area id "
                   "of this handle which will used by next action"},
 {"0~max-1",""     "add a data block to area, 0~max-1 means the area id for the stream."
                   " The return value is the data block index in the area"},
 {"0~max-1",""     "update a data block of this area, 0~max-1 "
                   "means the area id for the stream"},
 {"0~max-1",""     "remove a data block from a area, 0~max-1 means the area id"
                   " for the stream"},
 {"0~max-1","\t"   "remove a area, 0~max-1 means the area id for the stream"},
 {"0~max-1","\t"   "enable a area, 0~max-1 means the area id for the stream"},
 {"0~max-1","\t"   "disable a area, 0~max-1 means the area id for the stream"},
 {"",     "\t\t"   "show a area parameters"},
 {"",     "\t\t"   "save all setting which are using to configure file"
                   "(/etc/oryx/video/osd_overlay.acs)"},
 {"",     "\t\t"   "delete all overlay for all stream"},
 {"",     "\t\t\t" "area or data block width"},
 {"",     "\t\t\t" "area or data block height"},
 {"",     "\t\t\t" "area or data block offset x"},
 {"",     "\t\t\t" "area or data block offset y"},
 {"",     "\t"     "area buffer number, it is useful when data type is animation"
                   " or picture which frequently do update manipulation!\n\t\t\t\t\t"
                   "Note: Don't put more than one Animation data in one area "
                   "when buffer number > 1!"},
 {"",     "\t\t"   "area background color, v:24-31,u:16-23,y:8-15,0-7:alpha"},
 {"",     "\t"     "When rotate clockwise degree is 90/180/270, area whether "
                   "consistent with stream orientation."},
 {"",     "\t\t"   "data block type for add or update to area, 0:string; 1:picture;"
                   " 2:time(can't do update); 3:animation(can't do update)"},
 {"",     "\t\t"   "data block index in area, which will used in "
                   "data block update and data block remove"},
 {"",     "\t\t"   "string to be inserted when data block type is string"},
 {"",     "\t"     "prefix string to be inserted when data block type is time"},
 {"",     "\t"     "suffix string to be inserted when data block type is time"},
 {"",     "\t"     "char spacing when data block type is string or time"},
 {"",     "\t\t"   "data block(which is string or time) background color,"
                   " v:24-31,u:16-23,y:8-15,0-7:alpha"},
 {"",     "\t\t"   "whether enable msec display when data block is time,"
                   "0:disable;1:enable"},
 {"",     "\t\t"   "font type name"},
 {"",     "\t\t"   "font size"},
 {"",     "\t\t"   "font color,0-7 is predefine color: 0,white;1,black;2,red;"
                   "3,blue;4,green;5,yellow;\n\t\t\t\t\t6,cyan;7,magenta; "
                   ">7,user custom(v:24-31,u:16-23,y:8-15,0-7:alpha)"},
 {"",     "\t\t"   "outline size"},
 {"",     "\t\t"   "outline color, v:24-31,u:16-23,y:8-15,0-7:alpha"},
 {"",     "\t\t"   "n percentage HOR bold of font size"},
 {"",     "\t\t"   "n percentage VER bold of font size"},
 {"",     "\t\t"   "n percentage italic of font size"},
 {"",     "\t\t"   "bmp file to be inserted when data block type is picture"},
 {"",     "\t\t"   "bmp number in the file when data block type is animation"},
 {"",     "\t"     "frame interval to display picture when data block type is animation"},
 {"",     "\t\t"   "color used to transparent when data block type is picture,"
                   "v:24-31,u:16-23,y:8-15,a:0-7"},
 {"",     "\t\t"   "color range used to transparent with color key\n"},

 {"0|1",  "\t\t"   "low bitrate enable"},
 {"0|1",  "\t\t"   "low bitrate auto bitrate target"},
 {"", "\t\t\t"     "low bitrate bitrate ceiling(bps/MB)"},
 {"0|1",  "\t\t"   "low bitrate frame drop\n"},

 {"",     "\t\t\t" "EIS mode\n"},

 {"",     "\t\t" "bitrate get"},
 {"",     "\t\t" "bitrate set"},
 {"",     "\t\t" "frame factor get"},
 {"",     "\t\t" "frame factor set mul/div"},
 {"",     "\t\t" "mjpeg quality get"},
 {"",     "\t\t" "mjpeg quality set"},
 {"",     "\t\t" "h264 gop get"},
 {"",     "\t\t" "h264 gop N set"},
 {"",     "\t\t" "h264 gop idr set"},
 {"",     "\t\t" "stream offset get"},
 {"",     "\t\t" "stream offset x set"},
 {"",     "\t\t" "stream offset y set"},

 {"",     "\t\t\t" "Pause streaming"},
 {"",     "\t\t\t" "Resume streaming"},

 {"",     "\t\t\t" "Restart streaming, but do not reload configs\n"},

 {"",     "\t"     "Show all streams' status"},
 {"",     "\t\t"   "Show Warp configs"},
 {"",     "\t\t"   "Show DPTZ ratio configs"},
 {"",     "\t\t"   "Show DPTZ size configs"},
 {"",     "\t\t"   "Show lbr configs"},
 {"",     "\t\t"   "Show EIS configs"},
 {"",     "\t\t"   "Show all information"}
};

static void usage(int32_t argc, char **argv)
{
  printf("\n%s usage:\n\n", argv[0]);
  for (uint32_t i = 0; i < sizeof(long_options)/sizeof(long_options[0])-1; ++i) {
    if (isalpha(long_options[i].val)) {
      printf("-%c, ", long_options[i].val);
    } else {
      printf("    ");
    }
    printf("--%s", long_options[i].name);
    if (hint32_t[i].arg[0] != 0) {
      printf(" [%s]", hint32_t[i].arg);
    }
    printf("\t%s\n", hint32_t[i].str);
  }
  printf("\n");
}

static int32_t init_param(int32_t argc, char **argv)
{
  int32_t ch;
  int32_t option_index = 0;
  int32_t ret = 0;
  int32_t val = 0;
  opterr = 0;

  while ((ch = getopt_long(argc, argv,
                           short_options,
                           long_options,
                           &option_index)) != -1) {
    switch (ch) {
      case 'h':
        usage(argc, argv);
        return -1;

      case VOUT_HALT:
        VERIFY_PARA_2(atoi(optarg), 0, 1);
        vout_stop.value.v_int = atoi(optarg);
        vout_stop.is_set = true;
        break;
      case BUFFER_NUM_MAX:
        get_platform_buffer_num_max_flag = true;
        break;
      case STREAM_NUM_MAX:
        get_platform_stream_num_max_flag = true;
        break;
      case 's':
        current_stream_id = AM_STREAM_ID(atoi(optarg));
        break;

      case FORCE_IDR:
        force_idr_id |= (1 << current_stream_id);
        break;
      case LDC_STRENGTH:
        current_buffer_id = AM_SOURCE_BUFFER_MAIN;
        VERIFY_PARA_2_FLOAT(atof(optarg), 0.0, 20.0);
        g_warp_cfg.ldc_strength.value.v_float = atof(optarg);
        g_warp_cfg.ldc_strength.is_set = true;
        g_warp_cfg.is_set = true;
        break;
      case PANO_HFOV_DEGREE:
        current_buffer_id = AM_SOURCE_BUFFER_MAIN;
        VERIFY_PARA_2_FLOAT(atof(optarg), 1.0, 180.0);
        g_warp_cfg.pano_hfov_degree.value.v_float = atof(optarg);
        g_warp_cfg.pano_hfov_degree.is_set = true;
        g_warp_cfg.is_set = true;
        break;
      case WARP_REGION_YAW:
        current_buffer_id = AM_SOURCE_BUFFER_MAIN;
        VERIFY_PARA_2(atoi(optarg), -90, 90);
        g_warp_cfg.warp_region_yaw.value.v_int = atoi(optarg);
        g_warp_cfg.warp_region_yaw.is_set = true;
        g_warp_cfg.is_set = true;
        break;
      case PAN_RATIO:
        VERIFY_PARA_2_FLOAT(atof(optarg), -1.0, 1.0);
        g_dptz_ratio_cfg[current_buffer_id].pan.value.v_float = atof(optarg);
        g_dptz_ratio_cfg[current_buffer_id].pan.is_set = true;
        g_dptz_ratio_cfg[current_buffer_id].is_set = true;
        set_dptz_ratio_flag = true;
        break;
      case TILT_RATIO:
        VERIFY_PARA_2_FLOAT(atof(optarg), -1.0, 1.0);
        g_dptz_ratio_cfg[current_buffer_id].tilt.value.v_float = atof(optarg);
        g_dptz_ratio_cfg[current_buffer_id].tilt.is_set = true;
        g_dptz_ratio_cfg[current_buffer_id].is_set = true;
        set_dptz_ratio_flag = true;
        break;
      case ZOOM_RATIO:
        VERIFY_PARA_1_FLOAT(atof(optarg), 0.1);
        g_dptz_ratio_cfg[current_buffer_id].zoom.value.v_float = atof(optarg);
        g_dptz_ratio_cfg[current_buffer_id].zoom.is_set = true;
        g_dptz_ratio_cfg[current_buffer_id].is_set = true;
        set_dptz_ratio_flag = true;
        break;
      case DPTZ_X:
        VERIFY_PARA_1(atoi(optarg), 0);
        g_dptz_size_cfg[current_buffer_id].x.value.v_int = atoi(optarg);
        g_dptz_size_cfg[current_buffer_id].x.is_set = true;
        g_dptz_size_cfg[current_buffer_id].is_set = true;
        set_dptz_size_flag = true;
        break;
      case DPTZ_Y:
        VERIFY_PARA_1(atoi(optarg), 0);
        g_dptz_size_cfg[current_buffer_id].y.value.v_int = atoi(optarg);
        g_dptz_size_cfg[current_buffer_id].y.is_set = true;
        g_dptz_size_cfg[current_buffer_id].is_set = true;
        set_dptz_size_flag = true;
        break;
      case DPTZ_W:
        VERIFY_PARA_1(atoi(optarg), 0);
        g_dptz_size_cfg[current_buffer_id].w.value.v_int = atoi(optarg);
        g_dptz_size_cfg[current_buffer_id].w.is_set = true;
        g_dptz_size_cfg[current_buffer_id].is_set = true;
        set_dptz_size_flag = true;
        break;
      case DPTZ_H:
        VERIFY_PARA_1(atoi(optarg), 0);
        g_dptz_size_cfg[current_buffer_id].h.value.v_int = atoi(optarg);
        g_dptz_size_cfg[current_buffer_id].h.is_set = true;
        g_dptz_size_cfg[current_buffer_id].is_set = true;
        set_dptz_size_flag = true;
        break;

      case 'b':
        current_buffer_id = AM_SOURCE_BUFFER_ID(atoi(optarg));
        break;

        //overlay parameter
      case OVERLAY_MAX:
        get_overlay_max_flag = true;
        break;
      case OVERLAY_SAVE:
        save_overlay_flag = true;
        break;
      case OVERLAY_DESTROY:
        destroy_overlay_flag = true;
        break;
      case OVERLAY_INIT:
        g_overlay_setting[current_stream_id].is_set = true;
        g_overlay_setting[current_stream_id].init.is_set = true;
        break;
      case OVERLAY_DATA_ADD:
        g_overlay_setting[current_stream_id].is_set = true;
        g_overlay_setting[current_stream_id].add_data.is_set = true;
        g_overlay_setting[current_stream_id].add_data.area_id = atoi(optarg);
        break;
      case OVERLAY_DATA_UPDATE:
        g_overlay_setting[current_stream_id].is_set = true;
        g_overlay_setting[current_stream_id].update_data.is_set = true;
        g_overlay_setting[current_stream_id].update_data.area_id = atoi(optarg);
        break;
      case OVERLAY_DATA_REMOVE:
        g_overlay_setting[current_stream_id].is_set = true;
        g_overlay_setting[current_stream_id].remove_data.is_set = true;
        g_overlay_setting[current_stream_id].remove_data.area_id = atoi(optarg);
        break;
      case OVERLAY_REMOVE:
        g_overlay_setting[current_stream_id].is_set = true;
        g_overlay_setting[current_stream_id].remove.is_set = true;
        g_overlay_setting[current_stream_id].remove.area_id = atoi(optarg);
        break;
      case OVERLAY_ENABLE:
        g_overlay_setting[current_stream_id].is_set = true;
        g_overlay_setting[current_stream_id].enable.is_set = true;
        g_overlay_setting[current_stream_id].enable.area_id = atoi(optarg);
        break;
      case OVERLAY_DISABLE:
        g_overlay_setting[current_stream_id].is_set = true;
        g_overlay_setting[current_stream_id].disable.is_set = true;
        g_overlay_setting[current_stream_id].disable.area_id = atoi(optarg);
        break;
      case OVERLAY_SHOW:
        g_overlay_setting[current_stream_id].is_set = true;
        g_overlay_setting[current_stream_id].show.is_set = true;
        g_overlay_setting[current_stream_id].show.area_id = atoi(optarg);
        break;
      case OVERLAY_AREA_BUF_NUM:
        g_overlay_setting[current_stream_id].init.attr.is_set = true;
        g_overlay_setting[current_stream_id].init.attr.buf_num.is_set = true;
        g_overlay_setting[current_stream_id].init.attr.buf_num.value.v_int =
            atoi(optarg);
        break;
      case OVERLAY_AREA_ROTATE:
        g_overlay_setting[current_stream_id].init.attr.is_set = true;
        g_overlay_setting[current_stream_id].init.attr.rotate.is_set = true;
        g_overlay_setting[current_stream_id].init.attr.rotate.value.v_int =
            atoi(optarg);
        break;
      case OVERLAY_AREA_BG_COLOR:
        g_overlay_setting[current_stream_id].init.attr.is_set = true;
        g_overlay_setting[current_stream_id].init.attr.bg_color.is_set = true;
        g_overlay_setting[current_stream_id].init.attr.bg_color.value.v_uint =
            (uint32_t)atoll(optarg);
        break;
      case OVERLAY_W:
        val = atoi(optarg);
        if (g_overlay_setting[current_stream_id].init.is_set) {
          g_overlay_setting[current_stream_id].init.attr.is_set = true;
          g_overlay_setting[current_stream_id].init.attr.w.is_set = true;
          g_overlay_setting[current_stream_id].init.attr.w.value.v_int = val;
        } else if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.w.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.w.value.v_int = val;
        }
        break;
      case OVERLAY_H:
        val = atoi(optarg);
        if (g_overlay_setting[current_stream_id].init.is_set) {
          g_overlay_setting[current_stream_id].init.attr.is_set = true;
          g_overlay_setting[current_stream_id].init.attr.h.is_set = true;
          g_overlay_setting[current_stream_id].init.attr.h.value.v_int = val;
        } else if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.h.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.h.value.v_int = val;
        }
        break;
      case OVERLAY_X:
        val = atoi(optarg);
        if (g_overlay_setting[current_stream_id].init.is_set) {
          g_overlay_setting[current_stream_id].init.attr.is_set = true;
          g_overlay_setting[current_stream_id].init.attr.x.is_set = true;
          g_overlay_setting[current_stream_id].init.attr.x.value.v_int = val;
        } else if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.x.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.x.value.v_int = val;
        }
        break;
      case OVERLAY_Y:
        val = atoi(optarg);
        if (g_overlay_setting[current_stream_id].init.is_set) {
          g_overlay_setting[current_stream_id].init.attr.is_set = true;
          g_overlay_setting[current_stream_id].init.attr.y.is_set = true;
          g_overlay_setting[current_stream_id].init.attr.y.value.v_int = val;
        } else if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.y.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.y.value.v_int = val;
        }
        break;
      case OVERLAY_DATA_INDEX:
        if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data_idx = atoi(optarg);
        } else if (g_overlay_setting[current_stream_id].remove_data.is_set) {
          g_overlay_setting[current_stream_id].remove_data.data_idx = atoi(optarg);
        }
        break;
      case OVERLAY_DATA_TYPE:
        VERIFY_PARA_2(atoi(optarg), 0, 3);
        g_overlay_setting[current_stream_id].add_data.data.is_set = true;
        g_overlay_setting[current_stream_id].add_data.data.type.is_set = true;
        g_overlay_setting[current_stream_id].add_data.data.type.value.v_int =
            atoi(optarg);
        break;
      case OVERLAY_TEXT_STRING:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.str.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.str.str = optarg;
          g_overlay_setting[current_stream_id].add_data.data.str.
          str[OVERLAY_MAX_STRING-1] = '\0';
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.str.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.str.str = optarg;
          g_overlay_setting[current_stream_id].update_data.data.str.
          str[OVERLAY_MAX_STRING-1] = '\0';
        }
        break;
      case OVERLAY_TEXT_PREFIX_STRING:
        g_overlay_setting[current_stream_id].add_data.data.is_set = true;
        g_overlay_setting[current_stream_id].add_data.data.pre_str.is_set = true;
        g_overlay_setting[current_stream_id].add_data.data.pre_str.str = optarg;
        g_overlay_setting[current_stream_id].add_data.data.pre_str.
        str[OVERLAY_MAX_STRING-1] = '\0';
        break;
      case OVERLAY_TEXT_SUFFIX_STRING:
        g_overlay_setting[current_stream_id].add_data.data.is_set = true;
        g_overlay_setting[current_stream_id].add_data.data.suf_str.is_set = true;
        g_overlay_setting[current_stream_id].add_data.data.suf_str.str = optarg;
        g_overlay_setting[current_stream_id].add_data.data.suf_str.
        str[OVERLAY_MAX_STRING-1] = '\0';
        break;
      case OVERLAY_TEXT_MSEC_EN:
        g_overlay_setting[current_stream_id].add_data.data.is_set = true;
        g_overlay_setting[current_stream_id].add_data.data.en_msec.is_set = true;
        g_overlay_setting[current_stream_id].add_data.data.en_msec.value.v_int =
            atoi(optarg);
        break;
      case OVERLAY_TEXT_BG_COLOR:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.bg_color.is_set
          = true;
          g_overlay_setting[current_stream_id].add_data.data.bg_color.
          value.v_uint = (uint32_t)atoll(optarg);
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.bg_color.is_set
          = true;
          g_overlay_setting[current_stream_id].update_data.data.bg_color.
          value.v_uint = (uint32_t)atoll(optarg);
        }
        break;
      case OVERLAY_TEXT_SPACING:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.spacing.is_set =
              true;
          g_overlay_setting[current_stream_id].add_data.data.spacing.
          value.v_int = atoi(optarg);
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.spacing.is_set
          = true;
          g_overlay_setting[current_stream_id].update_data.data.spacing.
          value.v_int = atoi(optarg);
        }
        break;
      case OVERLAY_FONT_TYPE:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.font_name.is_set =
              true;
          g_overlay_setting[current_stream_id].add_data.data.font_name.str =
              optarg;
          g_overlay_setting[current_stream_id].add_data.data.font_name.
          str[OVERLAY_MAX_FILENAME-1] = '\0';
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_name.is_set
          = true;
          g_overlay_setting[current_stream_id].update_data.data.font_name.str =
              optarg;
          g_overlay_setting[current_stream_id].update_data.data.font_name.
          str[OVERLAY_MAX_FILENAME-1] = '\0';
        }
        break;
      case OVERLAY_FONT_SIZE:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.font_size.is_set =
              true;
          g_overlay_setting[current_stream_id].add_data.data.font_size.
          value.v_int = atoi(optarg);
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_size.is_set
          = true;
          g_overlay_setting[current_stream_id].update_data.data.font_size.
          value.v_int = atoi(optarg);
        }
        break;
      case OVERLAY_FONT_COLOR:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.font_color.is_set
          = true;
          g_overlay_setting[current_stream_id].add_data.data.font_color.
          value.v_uint = (uint32_t)atoll(optarg);
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_color.is_set
          = true;
          g_overlay_setting[current_stream_id].update_data.data.font_color.
          value.v_uint = (uint32_t)atoll(optarg);
        }
        break;
      case OVERLAY_FONT_OUTLINE_WIDTH:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.font_outline_w.
          is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.font_outline_w.
          value.v_int = atoi(optarg);
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_outline_w.
          is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_outline_w.
          value.v_int = atoi(optarg);
        }
        break;
      case OVERLAY_FONT_OUTLINE_COLOR:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.font_outline_color.
          is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.font_outline_color.
          value.v_uint = (uint32_t)atoll(optarg);
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_outline_color.
          is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_outline_color.
          value.v_uint = (uint32_t)atoll(optarg);
        }
        break;
      case OVERLAY_FONT_HOR_BOLD:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.font_hor_bold.is_set =
              true;
          g_overlay_setting[current_stream_id].add_data.data.font_hor_bold.
          value.v_int = atoi(optarg);
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_hor_bold.
          is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_hor_bold.
          value.v_int = atoi(optarg);
        }
        break;
      case OVERLAY_FONT_VER_BOLD:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.font_ver_bold.is_set =
              true;
          g_overlay_setting[current_stream_id].add_data.data.font_ver_bold.
          value.v_int = atoi(optarg);
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_ver_bold.
          is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_ver_bold.
          value.v_int = atoi(optarg);
        }
        break;
      case OVERLAY_FONT_ITALIC:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.font_italic.is_set
          = true;
          g_overlay_setting[current_stream_id].add_data.data.font_italic.
          value.v_int = atoi(optarg);
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_italic.
          is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.font_italic.
          value.v_int = atoi(optarg);
        }
        break;
      case OVERLAY_BMP:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.bmp.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.bmp.str = optarg;
          g_overlay_setting[current_stream_id].add_data.data.bmp.
          str[OVERLAY_MAX_FILENAME-1] = '\0';
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.bmp.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.bmp.str = optarg;
          g_overlay_setting[current_stream_id].update_data.data.bmp.
          str[OVERLAY_MAX_FILENAME-1] = '\0';
        }
        break;
      case OVERLAY_BMP_NUM:
        g_overlay_setting[current_stream_id].add_data.data.is_set = true;
        g_overlay_setting[current_stream_id].add_data.data.bmp_num.is_set = true;
        g_overlay_setting[current_stream_id].add_data.data.bmp_num.value.v_int
        = atoi(optarg);
        break;
      case OVERLAY_BMP_INTERVAL:
        g_overlay_setting[current_stream_id].add_data.data.is_set = true;
        g_overlay_setting[current_stream_id].add_data.data.interval.is_set = true;
        g_overlay_setting[current_stream_id].add_data.data.interval.value.v_int
        = atoi(optarg);
        break;
      case OVERLAY_COLOR_KEY:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.color_key.is_set
          = true;
          g_overlay_setting[current_stream_id].add_data.data.color_key.
          value.v_uint = (uint32_t)atoll(optarg);
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.color_key.is_set
          = true;
          g_overlay_setting[current_stream_id].update_data.data.color_key.
          value.v_uint = (uint32_t)atoll(optarg);
        }
        break;
      case OVERLAY_COLOR_RANGE:
        if (g_overlay_setting[current_stream_id].add_data.is_set) {
          g_overlay_setting[current_stream_id].add_data.data.is_set = true;
          g_overlay_setting[current_stream_id].add_data.data.color_range.is_set
          = true;
          g_overlay_setting[current_stream_id].add_data.data.color_range.
          value.v_int = atoi(optarg);
        } else if (g_overlay_setting[current_stream_id].update_data.is_set) {
          g_overlay_setting[current_stream_id].update_data.data.is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.color_range.
          is_set = true;
          g_overlay_setting[current_stream_id].update_data.data.color_range.
          value.v_int = atoi(optarg);
        }
        break;

      case LBR_ENABLE:
        VERIFY_PARA_1(atoi(optarg), 0);
        g_lbr_cfg[current_stream_id].is_set = true;
        g_lbr_cfg[current_stream_id].lbr_enable.value.v_bool = atoi(optarg);
        g_lbr_cfg[current_stream_id].lbr_enable.is_set = true;
        break;
      case LBR_AUTO_BITRATE_TARGET:
        VERIFY_PARA_1(atoi(optarg), 0);
        g_lbr_cfg[current_stream_id].is_set = true;
        g_lbr_cfg[current_stream_id].lbr_auto_bitrate_target.value.v_bool = atoi(optarg);
        g_lbr_cfg[current_stream_id].lbr_auto_bitrate_target.is_set = true;
        break;
      case LBR_BITRATE_CEILING:
        VERIFY_PARA_1(atoi(optarg), 0);
        g_lbr_cfg[current_stream_id].is_set = true;
        g_lbr_cfg[current_stream_id].lbr_bitrate_ceiling.value.v_int = atoi(optarg);
        g_lbr_cfg[current_stream_id].lbr_bitrate_ceiling.is_set = true;
        break;
      case LBR_FRAME_DROP:
        VERIFY_PARA_1(atoi(optarg), 0);
        g_lbr_cfg[current_stream_id].is_set = true;
        g_lbr_cfg[current_stream_id].lbr_frame_drop.value.v_bool = atoi(optarg);
        g_lbr_cfg[current_stream_id].lbr_frame_drop.is_set = true;
        break;

      case EIS_MODE:
        g_eis_cfg.is_set = true;
        g_eis_cfg.eis_mode.is_set = true;
        g_eis_cfg.eis_mode.value.v_int = atoi(optarg);
        break;
      case BITRATE_GET:
        show_bitrate_flag = true;
        break;
      case BITRATE_SET:
        g_bitrate_cfg.is_set = true;
        g_bitrate_cfg.target_bitrate.value.v_int = atoi(optarg);
        break;
      case FRAMEFACTOR_GET:
        show_framefactor_flag = true;
        break;
      case FRAMEFACTOR_SET:
        g_framefactor_cfg.is_set = true;
        char *cmul, *cdiv;
        int mul, div;
        if ((cdiv = strstr(optarg, "/")) == NULL) {
          break;
        }
        cdiv = cdiv + 1;
        cmul = optarg;
        mul = strtol(cmul, NULL, 10);
        div = strtol(cdiv, NULL, 10);
        g_framefactor_cfg.mul.value.v_int = mul;
        g_framefactor_cfg.div.value.v_int = div;
        break;
      case MJPEG_QUALITY_GET:
        show_mjpeg_flag = true;
        break;
      case MJPEG_QUALITY_SET:
        g_mjpeg_cfg.is_set = true;
        g_mjpeg_cfg.quality.value.v_int = atoi(optarg);
        break;
      case H264_GOP_GET:
        show_h264_gop_flag = true;
        break;
      case H264_GOP_N_SET:
        g_h264_gop_cfg.is_set = true;
        g_h264_gop_cfg.N.is_set = true;
        g_h264_gop_cfg.N.value.v_int = atoi(optarg);
        break;
      case H264_GOP_IDR_SET:
        g_h264_gop_cfg.is_set = true;
        g_h264_gop_cfg.idr.is_set = true;
        g_h264_gop_cfg.idr.value.v_int = atoi(optarg);
        break;
      case STREAM_OFFSET_GET:
        show_stream_offset_flag = true;
        break;
      case STREAM_OFFSET_X:
        g_stream_offset_cfg.is_set = true;
        g_stream_offset_cfg.x.is_set = true;
        g_stream_offset_cfg.x.value.v_int = atoi(optarg);
        break;
      case STREAM_OFFSET_Y:
        g_stream_offset_cfg.is_set = true;
        g_stream_offset_cfg.y.is_set = true;
        g_stream_offset_cfg.y.value.v_int = atoi(optarg);
        break;

      case STOP_ENCODING:
        pause_flag = true;
        break;
      case START_ENCODING:
        resume_flag = true;
        break;

      case APPLY:
        restart_flag = true;
        break;

      case SHOW_STREAM_STATUS:
        show_status_flag = true;
        break;
      case SHOW_WARP_CFG:
        show_warp_flag = true;
        break;
      case SHOW_DPTZ_RATIO_CFG:
        show_dptz_ratio_flag = true;
        break;
      case SHOW_DPTZ_SIZE_CFG:
        show_dptz_size_flag = true;
        break;
      case SHOW_LBR_CFG:
        show_lbr_flag = true;
        break;
      case SHOW_EIS_CFG:
        show_eis_flag = true;
        break;

      case SHOW_ALL_INFO:
        show_info_flag = true;
        break;
      default:
        ret = -1;
        printf("unknown option found: %d\n", ch);
        break;
    }
  }

  return ret;
}

static int32_t stop_encoding();

static int32_t stop_vout()
{
  int32_t ret = 0;
  do {
    if (vout_stop.is_set) {
      am_service_result_t service_result;
      int32_t vout_id = vout_stop.value.v_int;
      g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_VOUT_HALT,
                                &vout_id, sizeof(vout_id),
                                &service_result, sizeof(service_result));
      if ((ret = service_result.ret) != 0) {
        ERROR("failed to stop VOUT device!\n");
      }
    }
  } while(0);

  return ret;
}

static int32_t set_dptz_ratio()
{
  uint32_t ret = 0;
  am_service_result_t service_result;
  am_dptz_ratio_t dptz_cfg = {0};
  for (auto &m : g_dptz_ratio_cfg) {
    if (!(m.second.is_set)) {
      continue;
    }
    if (m.second.pan.is_set) {
      SET_BIT(dptz_cfg.enable_bits, AM_DPTZ_PAN_RATIO_EN_BIT);
      dptz_cfg.pan_ratio = m.second.pan.value.v_float;
    }
    if (m.second.tilt.is_set) {
      SET_BIT(dptz_cfg.enable_bits, AM_DPTZ_TILT_RATIO_EN_BIT);
      dptz_cfg.tilt_ratio = m.second.tilt.value.v_float;
    }
    if (m.second.zoom.is_set) {
      SET_BIT(dptz_cfg.enable_bits, AM_DPTZ_ZOOM_RATIO_EN_BIT);
      dptz_cfg.zoom_ratio = m.second.zoom.value.v_float;
    }
    dptz_cfg.buffer_id = m.first;
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_SET,
                              &dptz_cfg, sizeof(dptz_cfg),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("failed to set dptz_ratio\n");
    }
  }
  return ret;
}

static int32_t set_dptz_size()
{
  uint32_t ret = 0;
  am_service_result_t service_result;
  am_dptz_size_t dptz_cfg = {0};
  for (auto &m : g_dptz_size_cfg) {
    if (!(m.second.is_set)) {
      continue;
    }
    if (m.second.x.is_set) {
      SET_BIT(dptz_cfg.enable_bits, AM_DPTZ_SIZE_X_EN_BIT);
      dptz_cfg.x = m.second.x.value.v_int;
    }
    if (m.second.y.is_set) {
      SET_BIT(dptz_cfg.enable_bits, AM_DPTZ_SIZE_Y_EN_BIT);
      dptz_cfg.y = m.second.y.value.v_int;
    }
    if (m.second.w.is_set) {
      SET_BIT(dptz_cfg.enable_bits, AM_DPTZ_SIZE_W_EN_BIT);
      dptz_cfg.w = m.second.w.value.v_int;
    }
    if (m.second.h.is_set) {
      SET_BIT(dptz_cfg.enable_bits, AM_DPTZ_SIZE_H_EN_BIT);
      dptz_cfg.h = m.second.h.value.v_int;
    }
    dptz_cfg.buffer_id = m.first;
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_SET,
                              &dptz_cfg, sizeof(dptz_cfg),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("failed to set dptz size config!\n");
    }
  }
  return ret;
}
static int32_t set_warp_config()
{
  uint32_t ret = 0;
  am_service_result_t service_result;
  am_warp_t warp_cfg = {0};
  bool has_warp_setting = false;

  if (g_warp_cfg.ldc_strength.is_set) {
    SET_BIT(warp_cfg.enable_bits, AM_WARP_LDC_STRENGTH_EN_BIT);
    warp_cfg.ldc_strength = g_warp_cfg.ldc_strength.value.v_float;
    has_warp_setting = true;
  }
  if (g_warp_cfg.pano_hfov_degree.is_set) {
    SET_BIT(warp_cfg.enable_bits, AM_WARP_PANO_HFOV_DEGREE_EN_BIT);
    warp_cfg.pano_hfov_degree = g_warp_cfg.pano_hfov_degree.value.v_float;
    has_warp_setting = true;
  }
  if (g_warp_cfg.warp_region_yaw.is_set) {
    SET_BIT(warp_cfg.enable_bits, AM_WARP_REGION_YAW_EN_BIT);
    warp_cfg.warp_region_yaw = g_warp_cfg.warp_region_yaw.value.v_int;
    has_warp_setting = true;
  }
  if (has_warp_setting) {
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_WARP_SET,
                              &warp_cfg, sizeof(warp_cfg),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("failed to set warp config!\n");
    }
  }

  return ret;
}

static int32_t set_lbr_config()
{
  int32_t ret = 0;
  am_service_result_t service_result;
  am_encode_h264_lbr_ctrl_t lbr_cfg = {0};
  bool has_setting = false;
  for (uint32_t i = 0; i < MAX_STREAM_NUM; ++i) {
    if (!g_lbr_cfg[i].is_set) {
      continue;
    }
    lbr_cfg.stream_id = i;
    if (g_lbr_cfg[i].lbr_enable.is_set) {
      SET_BIT(lbr_cfg.enable_bits, AM_ENCODE_H264_LBR_ENABLE_LBR_EN_BIT);
      lbr_cfg.enable_lbr = g_lbr_cfg[i].lbr_enable.value.v_bool;
      has_setting = true;
    }

    if (g_lbr_cfg[i].lbr_auto_bitrate_target.is_set) {
      SET_BIT(lbr_cfg.enable_bits, AM_ENCODE_H264_LBR_AUTO_BITRATE_CEILING_EN_BIT);
      lbr_cfg.auto_bitrate_ceiling = g_lbr_cfg[i].lbr_auto_bitrate_target.value.v_bool;
      has_setting = true;
    }

    if (g_lbr_cfg[i].lbr_bitrate_ceiling.is_set) {
      SET_BIT(lbr_cfg.enable_bits, AM_ENCODE_H264_LBR_BITRATE_CEILING_EN_BIT);
      lbr_cfg.bitrate_ceiling = g_lbr_cfg[i].lbr_bitrate_ceiling.value.v_int;
      has_setting = true;
    }

    if (g_lbr_cfg[i].lbr_frame_drop.is_set) {
      SET_BIT(lbr_cfg.enable_bits, AM_ENCODE_H264_LBR_DROP_FRAME_EN_BIT);
      lbr_cfg.drop_frame = g_lbr_cfg[i].lbr_frame_drop.value.v_bool;
      has_setting = true;
    }
  }

  if (has_setting) {
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_LBR_SET,
                              &lbr_cfg, sizeof(lbr_cfg),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("failed to set lbr cfg config!\n");
    }
  }

  return ret;
}

static int32_t set_eis_config()
{
  int32_t ret = 0;
  am_service_result_t service_result;
  am_encode_eis_ctrl_t eis_config = {0};
  bool has_eis_setting = false;

  if (g_eis_cfg.eis_mode.is_set) {
    SET_BIT(eis_config.enable_bits, AM_ENCODE_EIS_MODE_EN_BIT);
    eis_config.eis_mode = g_eis_cfg.eis_mode.value.v_int;
    has_eis_setting = true;
  }

  if (has_eis_setting) {
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_EIS_SET,
                              &eis_config, sizeof(eis_config),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("failed to set eis config!\n");
    }
  }

  return ret;
}

static int32_t set_framefactor()
{
  int32_t ret = 0;
  am_service_result_t service_result;
  am_framefactor_t framefactor = {0};
  bool has_setting = false;

  if (g_framefactor_cfg.is_set) {
    SET_BIT(framefactor.enable_bits, AM_FRAMEFACTOR_EN_BIT);
    framefactor.stream_id = current_stream_id;
    framefactor.mul = g_framefactor_cfg.mul.value.v_int;
    framefactor.div = g_framefactor_cfg.div.value.v_int;
    has_setting = true;
  }

  if (has_setting) {
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_SET,
                              &framefactor, sizeof(framefactor),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("failed to set framefactor config!\n");
    }
  }

  return ret;
}

static int32_t show_framefactor_info()
{
  int32_t ret =0;
  uint32_t stream_id;
  do {
    am_framefactor_t *framefactor = nullptr;
    am_service_result_t service_result;
    for (uint32_t i = 0; i < MAX_STREAM_NUM; ++i) {
      stream_id = i;
      g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_GET,
                                &stream_id, sizeof(stream_id),
                                &service_result, sizeof(service_result));
      if ((ret = service_result.ret) != 0) {
        ERROR("Failed to get stream bitrate config!");
        break;
      }

      framefactor = (am_framefactor_t *)service_result.data;

      PRINTF("[Stream %d framefactor info]", stream_id);
      PRINTF("factor : %d/%d", framefactor->mul, framefactor->div);
    }

  } while(0);

  return ret;
}

static int32_t set_bitrate()
{
  int32_t ret = 0;
  am_service_result_t service_result;
  am_bitrate_t bitrate = {0};
  bool has_br_setting = false;

  if (g_bitrate_cfg.is_set) {
    SET_BIT(bitrate.enable_bits, AM_BITRATE_EN_BIT);
    bitrate.stream_id = current_stream_id;
    bitrate.target_bitrate = g_bitrate_cfg.target_bitrate.value.v_int;
    has_br_setting = true;
  }

  if (has_br_setting) {
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_SET,
                              &bitrate, sizeof(bitrate),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("failed to set bitrate config!\n");
    }
  }

  return ret;
}

static int32_t show_bitrate_info()
{
  int32_t ret =0;
  uint32_t stream_id;
  do {
    am_bitrate_t *bitrate = nullptr;
    am_service_result_t service_result;
    for (uint32_t i = 0; i < MAX_STREAM_NUM; ++i) {
      stream_id = i;
      g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_GET,
                                &stream_id, sizeof(stream_id),
                                &service_result, sizeof(service_result));
      if ((ret = service_result.ret) != 0) {
        ERROR("Failed to get stream bitrate config!");
        break;
      }

      bitrate = (am_bitrate_t *)service_result.data;

      PRINTF("[Stream %d bitrate info]", stream_id);
      PRINTF("rate_ctrl_mode: %d", bitrate->rate_control_mode);
      PRINTF("bitrate: %d", bitrate->target_bitrate);
    }

  } while(0);

  return ret;
}

static int32_t set_mjpeg_quality()
{
  int32_t ret = 0;
  am_service_result_t service_result;
  am_mjpeg_cfg_t mjpeg = {0};
  bool has_setting = false;

  if (g_mjpeg_cfg.is_set) {
    SET_BIT(mjpeg.enable_bits, AM_MJPEG_CFG_QUALITY_EN_BIT);
    mjpeg.stream_id = current_stream_id;
    mjpeg.quality = g_mjpeg_cfg.quality.value.v_int;
    has_setting = true;
  }

  if (has_setting) {
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_MJPEG_QUALITY_SET,
                              &mjpeg, sizeof(mjpeg),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("failed to set mjpeg quality!\n");
    }
  }

  return ret;
}

static int32_t show_mjpeg_info()
{
  int32_t ret =0;
  uint32_t stream_id;
  do {
    am_mjpeg_cfg_t *mjpeg = nullptr;
    am_service_result_t service_result;
    for (uint32_t i = 0; i < MAX_STREAM_NUM; ++i) {
      stream_id = i;
      g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_MJPEG_QUALITY_GET,
                                &stream_id, sizeof(stream_id),
                                &service_result, sizeof(service_result));
      if ((ret = service_result.ret) != 0) {
        ERROR("Failed to get mjpeg info!");
        break;
      }

      mjpeg = (am_mjpeg_cfg_t *)service_result.data;

      PRINTF("[Stream %d mjpeg info]", stream_id);
      PRINTF("quality: %d", mjpeg->quality);
    }
  } while(0);

  return ret;
}

static int32_t set_h264_gop()
{
  int32_t ret = 0;
  am_service_result_t service_result;
  am_h264_cfg_t h264 = {0};
  bool has_setting = false;

  if (g_h264_gop_cfg.is_set) {
    if (g_h264_gop_cfg.N.is_set) {
      SET_BIT(h264.enable_bits, AM_H264_CFG_N_EN_BIT);
      h264.stream_id = current_stream_id;
      h264.N = g_h264_gop_cfg.N.value.v_int;
    }
    if (g_h264_gop_cfg.idr.is_set) {
      SET_BIT(h264.enable_bits, AM_H264_CFG_IDR_EN_BIT);
      h264.stream_id = current_stream_id;
      h264.idr_interval = g_h264_gop_cfg.idr.value.v_int;
    }
    has_setting = true;
  }

  if (has_setting) {
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_H264_GOP_SET,
                              &h264, sizeof(h264),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("failed to set h264 gop!\n");
    }
  }

  return ret;
}

static int32_t show_h264_gop_info()
{
  int32_t ret =0;
  uint32_t stream_id;
  do {
    am_h264_cfg_t *h264 = nullptr;
    am_service_result_t service_result;
    for (uint32_t i = 0; i < MAX_STREAM_NUM; ++i) {
      stream_id = i;
      g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_H264_GOP_GET,
                                &stream_id, sizeof(stream_id),
                                &service_result, sizeof(service_result));
      if ((ret = service_result.ret) != 0) {
        ERROR("Failed to get h264 gop!");
        break;
      }

      h264 = (am_h264_cfg_t *)service_result.data;

      PRINTF("[Stream %d h264 gop info]", stream_id);
      PRINTF("N: %d", h264->N);
      PRINTF("idr: %d", h264->idr_interval);
    }
  } while(0);

  return ret;
}

static int32_t set_stream_offset()
{
  int32_t ret = 0;
  am_service_result_t service_result;
  am_stream_offset_t offset = {0};
  bool has_setting = false;

  if (g_stream_offset_cfg.is_set) {
    if (g_stream_offset_cfg.x.is_set) {
      SET_BIT(offset.enable_bits, AM_STREAM_OFFSET_X_EN_BIT);
      offset.stream_id = current_stream_id;
      offset.x = g_stream_offset_cfg.x.value.v_int;
    }
    if (g_stream_offset_cfg.y.is_set) {
      SET_BIT(offset.enable_bits, AM_STREAM_OFFSET_Y_EN_BIT);
      offset.stream_id = current_stream_id;
      offset.y = g_stream_offset_cfg.y.value.v_int;
    }
    has_setting = true;
  }

  if (has_setting) {
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_STREAM_OFFSET_SET,
                              &offset, sizeof(offset),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("failed to set stream offset!\n");
    }
  }

  return ret;
}

static int32_t show_stream_offset_info()
{
  int32_t ret =0;
  uint32_t stream_id;
  do {
    am_stream_offset_t *offset = nullptr;
    am_service_result_t service_result;
    for (uint32_t i = 0; i < MAX_STREAM_NUM; ++i) {
      stream_id = i;
      g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_STREAM_OFFSET_GET,
                                &stream_id, sizeof(stream_id),
                                &service_result, sizeof(service_result));
      if ((ret = service_result.ret) != 0) {
        ERROR("Failed to get stream offset!");
        break;
      }

      offset = (am_stream_offset_t *)service_result.data;

      PRINTF("[Stream %d offset info]", stream_id);
      PRINTF("offset: %dx%d", offset->x, offset->y);
    }
  } while(0);

  return ret;
}

static int32_t start_encoding()
{
  int32_t ret = 0;
  am_service_result_t service_result;
  g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_ENCODE_START, nullptr, 0,
                            &service_result, sizeof(service_result));
  if ((ret = service_result.ret) != 0) {
    ERROR("failed to start encoding!\n");
  }
  return ret;
}

static int32_t stop_encoding()
{
  int32_t ret = 0;
  am_service_result_t service_result;
  g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_ENCODE_STOP, nullptr, 0,
                            &service_result, sizeof(service_result));
  if ((ret = service_result.ret) != 0) {
    ERROR("failed to stop encoding!\n");
  }
  return ret;
}

static int32_t restart_encoding()
{
  int32_t ret = 0;

  do {
    if ((ret = stop_encoding()) < 0) {
      break;
    }
    if ((ret = start_encoding()) < 0) {
      break;
    }
  } while (0);
  return ret;
}

static int32_t show_stream_status()
{
  int32_t ret = 0;
  am_stream_status_t *status = nullptr;
  am_service_result_t service_result;

  g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_STREAM_STATUS_GET,
                            nullptr, 0,
                            &service_result, sizeof(service_result));
  if ((ret = service_result.ret) != 0) {
    ERROR("Failed to Get Stream format!");
    return -1;
  }
  status = (am_stream_status_t*)service_result.data;

  printf("\n");
  uint32_t encode_count = 0;
  for (uint32_t i = 0; i < MAX_STREAM_NUM; ++i) {
    if (TEST_BIT(status->status, i)) {
      ++encode_count;
      PRINTF("Stream[%d] is encoding...", i);
    }
  }
  printf("\n");
  if (encode_count == 0) {
    PRINTF("No stream in encoding!");
  }

  return ret;
}

static int32_t show_dptz_ratio()
{
  int32_t ret = 0;
  uint32_t buffer_id;
  am_dptz_ratio_t *cfg = NULL;
  am_service_result_t service_result;
  for (uint32_t i = 0; i < SOURCE_BUFFER_MAX_NUM; ++i) {
    buffer_id = i;
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_GET,
                              &buffer_id, sizeof(buffer_id),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("Failed to get dptz config!");
      break;
    }
    cfg = (am_dptz_ratio_t*)service_result.data;
    PRINTF("[Buffer%d dptz config]", buffer_id);
    PRINTF("pan_ratio:\t\t%.02f", cfg->pan_ratio);
    PRINTF("tilt_ratio:\t\t%.02f", cfg->tilt_ratio);
    PRINTF("zoom_ratio:\t\t%.02f", cfg->zoom_ratio);
    PRINTF("\n");
  }
  return ret;
}

static int32_t show_dptz_size()
{
  int32_t ret = 0;
  uint32_t buffer_id;
  am_dptz_size_t *cfg = NULL;
  am_service_result_t service_result;
  for (uint32_t i = 0; i < SOURCE_BUFFER_MAX_NUM; ++i) {
    buffer_id = i;
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_GET,
                              &buffer_id, sizeof(buffer_id),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("Failed to get dptz config!");
      break;
    }
    cfg = (am_dptz_size_t*)service_result.data;
    PRINTF("[Buffer%d dptz config]", buffer_id);
    PRINTF("offset.x:\t\t\t%d", cfg->x);
    PRINTF("offset.y:\t\t\t%d", cfg->y);
    PRINTF("size.width:\t\t%d", cfg->w);
    PRINTF("size.height:\t\t%d", cfg->h);
    PRINTF("\n");
  }
  return ret;
}
static int32_t show_warp_cfg()
{
  int32_t ret = 0;
  uint32_t buffer_id;
  do {
    am_warp_t *cfg = NULL;
    am_service_result_t service_result;
    buffer_id = 0;
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_WARP_GET,
                              &buffer_id, sizeof(buffer_id),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("Failed to get warp config!");
      break;
    }
    cfg = (am_warp_t*)service_result.data;
    PRINTF("[warp config]");
    PRINTF("ldc_strength:\t\t%.02f", cfg->ldc_strength);
    PRINTF("pano_hfov_degree:\t%.02f", cfg->pano_hfov_degree);
  } while (0);

  return ret;
}

static int32_t show_lbr_cfg()
{
  int32_t ret = 0;
  uint32_t stream_id;
  am_encode_h264_lbr_ctrl_t *cfg = NULL;
  am_service_result_t service_result;

  for (uint32_t i = 0; i < MAX_STREAM_NUM; ++i) {
    stream_id = i;
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_LBR_GET,
                              &stream_id, sizeof(stream_id),
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("Failed to get lbr config!");
      break;
    }

    cfg = (am_encode_h264_lbr_ctrl_t*)service_result.data;

    PRINTF("\n");
    PRINTF("[Stream%d LBR Config]", stream_id);
    PRINTF("enable_lbr:\t\t%d", cfg->enable_lbr);
    PRINTF("auto_bitrate_ceiling:\t%d", cfg->auto_bitrate_ceiling);
    PRINTF("bitrate_ceiling:\t%d", cfg->bitrate_ceiling);
    PRINTF("drop_frame:\t\t%d", cfg->drop_frame);
  }
  return ret;
}

static int32_t show_eis_cfg()
{
  int32_t ret =0;
  do {
    am_encode_eis_ctrl_t *cfg = nullptr;
    am_service_result_t service_result;

    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_EIS_GET,
                              nullptr, 0,
                              &service_result, sizeof(service_result));
    if ((ret = service_result.ret) != 0) {
      ERROR("Failed to get eis config!");
      break;
    }

    cfg = (am_encode_eis_ctrl_t*)service_result.data;

    PRINTF("[EIS Config]");
    PRINTF("EIS mode is now %d",cfg->eis_mode);

  } while(0);

  return ret;
}

static int32_t set_force_idr()
{
  int32_t ret = 0;
  am_service_result_t service_result;

  for (int32_t i = 0; i < MAX_STREAM_NUM; i++) {
    if (force_idr_id & (1 << i)) {
      g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_FORCE_IDR,
                                &i, sizeof(int32_t),
                                &service_result, sizeof(service_result));
      if ((ret = service_result.ret) != 0) {
        ERROR("Failed to set stream%d to force idr!");
        break;
      }
    }
  }

  return ret;
}

static void show_buffer_number_max()
{
  am_service_result_t service_result;
  g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_BUFFER_MAX_NUM_GET,
                            nullptr, 0,
                            &service_result, sizeof(service_result));
  if (service_result.ret != 0) {
    ERROR("failed to get platform max buffer number!\n");
    return;
  }
  uint32_t *val = (uint32_t*)service_result.data;
  PRINTF("The platform support max %d buffer!\n", *val);
}

static void show_stream_number_max()
{
  am_service_result_t service_result;
  g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_STREAM_MAX_NUM_GET,
                            nullptr, 0,
                            &service_result, sizeof(service_result));
  if (service_result.ret != 0) {
    ERROR("failed to get platform max stream number!\n");
    return;
  }
  uint32_t *val = (uint32_t*)service_result.data;
  PRINTF("The platform support max %d streams!\n", *val);
}

static void show_overlay_max()
{
  am_service_result_t service_result;
  g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET,
                            nullptr, 0,
                            &service_result, sizeof(service_result));
  if (service_result.ret != 0) {
    ERROR("failed to get overlay max number!\n");
    return;
  }
  am_overlay_limit_val_t *val = (am_overlay_limit_val_t*)service_result.data;
  PRINTF("The platform support max %d streams for overlay!\n",
         val->platform_stream_num_max);
  PRINTF("The platform support max %d overlay areas for a stream!\n",
         val->platform_overlay_area_num_max);
  PRINTF("The user defined max %d streams for overlay!\n",
         val->user_def_stream_num_max);
  PRINTF("The user defined max %d overlay areas for a stream!\n",
         val->user_def_overlay_area_num_max);
}

static void get_overlay_parameter(uint32_t stream_id,
                                  const overlay_area_setting &setting)
{
  do {
    if (!setting.is_set) {
      break;
    }
    am_overlay_id_t input_para = {0};
    am_service_result_t service_result;

    input_para.stream_id = stream_id;
    input_para.area_id = setting.area_id;
    g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET,
                              &input_para, sizeof(input_para),
                              &service_result, sizeof(service_result));
    if (service_result.ret != 0) {
      ERROR("failed to get overlay config!\n");
      break;
    }
    am_overlay_area_t *area = (am_overlay_area_t*)service_result.data;

    if ((area->enable !=0) && (area->enable != 1)) {
      PRINTF("[Stream %d overlay area%d: (not created)]\n",
             stream_id, input_para.area_id);
      break;
    }
    PRINTF("[Stream %d overlay area%d: (%s)]\n", stream_id, input_para.area_id,
           area->enable == 1 ? "enabled" : "disabled");
    PRINTF("    rotate:\t\t\t%d\n", area->rotate);
    PRINTF("    background color:\t\t0x%x\n", area->bg_color);
    PRINTF("    area size:\t\t\t%d x %d\n", area->width, area->height);
    PRINTF("    area offset:\t\t%d x %d\n", area->offset_x, area->offset_y);
    PRINTF("    area buffer number:\t\t%d\n", area->buf_num);
    PRINTF("    data block number:\t\t%d\n", area->num);

    int32_t n = area->num;
    for (int32_t i = 0; i < n; ++i) {
      input_para.data_index = i;
      g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET,
                                &input_para, sizeof(input_para),
                                &service_result, sizeof(service_result));
      if (service_result.ret != 0) {
        ERROR("failed to get overlay data parameter!\n");
        break;
      }
      am_overlay_data_t *data = (am_overlay_data_t*)service_result.data;

      PRINTF("\n");
      if (data->type > 3) {
        PRINTF("    data block%d: (not created)", i);
        ++n;
        continue;
      }

      PRINTF("    data block%d: (%s)\n", i, (data->type == 0) ? "string" :
          ((data->type == 1) ? "picture" : ((data->type == 2) ? "time" :
              "animation")));
      PRINTF("      block size:\t\t%d x %d\n", data->width, data->height);
      PRINTF("      block offset:\t\t%d x %d\n", data->offset_x, data->offset_y);
      if (0 == data->type || 2 == data->type) {
        PRINTF("      spacing:\t\t\t%d\n", data->spacing);
        PRINTF("      font size:\t\t%d\n", data->font_size);
        PRINTF("      background color:\t\t0x%x\n", data->bg_color);
        if (data->font_color < 8){
          PRINTF("      font color:\t\t%d\n", data->font_color);
        } else {
          PRINTF("      font color:\t\t0x%x\n", data->font_color);
        }
        PRINTF("      font outline_width:\t%d\n", data->font_outline_w);
        PRINTF("      font outline color:\t0x%x\n", data->font_outline_color);
        PRINTF("      font hor_bold:\t\t%d\n", data->font_hor_bold);
        PRINTF("      font ver_bold:\t\t%d\n", data->font_ver_bold);
        PRINTF("      font italic:\t\t%d\n", data->font_italic);
        PRINTF("      msec:\t\t\t%d\n", data->msec_en);
      }
      if (1 == data->type || 3 == data->type) {
        PRINTF("      color key:\t\t0x%x\n", data->color_key);
        PRINTF("      color range:\t\t%d\n", data->color_range);
        PRINTF("      bmp number:\t\t%d\n", data->bmp_num);
        PRINTF("      frame interval:\t\t%d\n", data->interval);
      }
    }
  } while(0);

  return;
}

static void init_overlay_area(uint32_t stream_id,
                              const overlay_area_setting &setting)
{
  if (!setting.is_set) {
    return;
  }
  am_service_result_t service_result = {0};
  am_overlay_area_t area = {0};

  SET_BIT(area.enable_bits, AM_OVERLAY_INIT_EN_BIT);
  area.stream_id = stream_id;
  if (setting.attr.x.is_set) {
    SET_BIT(area.enable_bits, AM_OVERLAY_RECT_EN_BIT);
    area.offset_x = setting.attr.x.value.v_int;
  }

  if (setting.attr.y.is_set) {
    SET_BIT(area.enable_bits, AM_OVERLAY_RECT_EN_BIT);
    area.offset_y = setting.attr.y.value.v_int;
  }
  if (setting.attr.w.is_set) {
    SET_BIT(area.enable_bits, AM_OVERLAY_RECT_EN_BIT);
    area.width = setting.attr.w.value.v_int;
  }
  if (setting.attr.h.is_set) {
    SET_BIT(area.enable_bits, AM_OVERLAY_RECT_EN_BIT);
    area.height = setting.attr.h.value.v_int;
  }
  if (setting.attr.rotate.is_set) {
    SET_BIT(area.enable_bits, AM_OVERLAY_ROTATE_EN_BIT);
    area.rotate = setting.attr.rotate.value.v_int;
  }
  if (setting.attr.bg_color.is_set) {
    SET_BIT(area.enable_bits, AM_OVERLAY_BG_COLOR_EN_BIT);
    area.bg_color = setting.attr.bg_color.value.v_uint;
  }
  if (setting.attr.buf_num.is_set) {
    SET_BIT(area.enable_bits, AM_OVERLAY_BUF_NUM_EN_BIT);
    area.buf_num = setting.attr.buf_num.value.v_int;
  }

  g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT,
                            &area, sizeof(area),
                            &service_result, sizeof(service_result));
  if (service_result.ret != 0) {
    ERROR("failed to init overlay area!\n");
  } else {
    int32_t *area_id = (int32_t*)service_result.data;
    PRINTF("The overlay area id = %d\n", *area_id);
  }
}

static void add_data_to_area(uint32_t stream_id,
                             const overlay_area_setting &setting)
{
  if (!setting.is_set) {
    return;
  }
  am_service_result_t service_result = {0};
  am_overlay_data_t  data = {0};

  SET_BIT(data.enable_bits, AM_OVERLAY_DATA_ADD_EN_BIT);
  data.id.stream_id = stream_id;
  data.id.area_id = setting.area_id;
  if (setting.data.x.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_RECT_EN_BIT);
    data.offset_x = setting.data.x.value.v_int;
  }
  if (setting.data.y.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_RECT_EN_BIT);
    data.offset_y = setting.data.y.value.v_int;
  }
  if (setting.data.w.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_RECT_EN_BIT);
    data.width = setting.data.w.value.v_int;
  }
  if (setting.data.h.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_RECT_EN_BIT);
    data.height = setting.data.h.value.v_int;
  }
  if (setting.data.type.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_DATA_TYPE_EN_BIT);
    data.type = setting.data.type.value.v_int;
  }
  if (setting.data.str.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_STRING_EN_BIT);
    snprintf(data.str, OVERLAY_MAX_STRING, "%s", setting.data.str.str.c_str());
  }
  if (setting.data.pre_str.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_TIME_EN_BIT);
    snprintf(data.pre_str, OVERLAY_MAX_STRING, "%s", setting.data.pre_str.str.
             c_str());
  }
  if (setting.data.suf_str.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_TIME_EN_BIT);
    snprintf(data.suf_str, OVERLAY_MAX_STRING, "%s", setting.data.suf_str.str.
             c_str());
  }
  if (setting.data.en_msec.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_TIME_EN_BIT);
    data.msec_en = setting.data.en_msec.value.v_int;
  }
  if (setting.data.spacing.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_CHAR_SPACING_EN_BIT);
    data.spacing = setting.data.spacing.value.v_int;
  }
  if (setting.data.bg_color.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_BG_COLOR_EN_BIT);
    data.bg_color = setting.data.bg_color.value.v_uint;
  }
  if (setting.data.font_name.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_TYPE_EN_BIT);
    snprintf(data.font_type, OVERLAY_MAX_FILENAME, "%s",
             setting.data.font_name.str.c_str());
  }
  if (setting.data.font_color.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_COLOR_EN_BIT);
    data.font_color = setting.data.font_color.value.v_uint;
  }
  if (setting.data.font_size.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_SIZE_EN_BIT);
    data.font_size = setting.data.font_size.value.v_int;
  }
  if (setting.data.font_outline_w.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_OUTLINE_EN_BIT);
    data.font_outline_w = setting.data.font_outline_w.value.v_int;
  }
  if (setting.data.font_outline_color.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_OUTLINE_EN_BIT);
    data.font_outline_color = setting.data.font_outline_color.value.v_uint;
  }
  if (setting.data.font_hor_bold.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_BOLD_EN_BIT);
    data.font_hor_bold = setting.data.font_hor_bold.value.v_int;
  }
  if (setting.data.font_ver_bold.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_BOLD_EN_BIT);
    data.font_ver_bold = setting.data.font_ver_bold.value.v_int;
  }
  if (setting.data.font_italic.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_ITALIC_EN_BIT);
    data.font_italic = setting.data.font_italic.value.v_int;
  }
  if (setting.data.bmp.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_BMP_EN_BIT);
    snprintf(data.bmp, OVERLAY_MAX_FILENAME, "%s", setting.data.bmp.str.c_str());
  }
  if (setting.data.bmp_num.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_ANIMATION_EN_BIT);
    data.bmp_num = setting.data.bmp_num.value.v_int;
  }
  if (setting.data.interval.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_ANIMATION_EN_BIT);
    data.interval = setting.data.interval.value.v_int;
  }
  if (setting.data.color_key.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_BMP_COLOR_EN_BIT);
    data.color_key = setting.data.color_key.value.v_uint;
  }
  if (setting.data.color_range.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_BMP_COLOR_EN_BIT);
    data.color_range = setting.data.color_range.value.v_int;
  }

  g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD,
                            &data, sizeof(data),
                            &service_result, sizeof(service_result));
  if (service_result.ret != 0) {
    ERROR("failed to add data to area!\n");
  } else {
    int32_t *data_index = (int32_t*)service_result.data;
    PRINTF("The data block index = %d\n", *data_index);
  }
}

static void update_area_data(uint32_t stream_id,
                             const overlay_area_setting &setting)
{
  if (!setting.is_set) {
    return;
  }
  am_service_result_t service_result = {0};
  am_overlay_data_t  data = {0};

  SET_BIT(data.enable_bits, AM_OVERLAY_DATA_UPDATE_EN_BIT);
  data.id.stream_id = stream_id;
  data.id.area_id = setting.area_id;
  data.id.data_index = setting.data_idx;
  if (setting.data.str.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_STRING_EN_BIT);
    snprintf(data.str, OVERLAY_MAX_STRING, "%s", setting.data.str.str.c_str());
  }
  if (setting.data.spacing.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_CHAR_SPACING_EN_BIT);
    data.spacing = setting.data.spacing.value.v_int;
  }
  if (setting.data.font_name.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_TYPE_EN_BIT);
    snprintf(data.font_type, OVERLAY_MAX_FILENAME, "%s",
             setting.data.font_name.str.c_str());
  }
  if (setting.data.bg_color.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_BG_COLOR_EN_BIT);
    data.bg_color = setting.data.bg_color.value.v_uint;
  }
  if (setting.data.font_color.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_COLOR_EN_BIT);
    data.font_color = setting.data.font_color.value.v_uint;
  }
  if (setting.data.font_size.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_SIZE_EN_BIT);
    data.font_size = setting.data.font_size.value.v_int;
  }
  if (setting.data.font_outline_w.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_OUTLINE_EN_BIT);
    data.font_outline_w = setting.data.font_outline_w.value.v_int;
  }
  if (setting.data.font_outline_color.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_OUTLINE_EN_BIT);
    data.font_outline_color = setting.data.font_outline_color.value.v_uint;
  }
  if (setting.data.font_hor_bold.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_BOLD_EN_BIT);
    data.font_hor_bold = setting.data.font_hor_bold.value.v_int;
  }
  if (setting.data.font_ver_bold.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_BOLD_EN_BIT);
    data.font_ver_bold = setting.data.font_ver_bold.value.v_int;
  }
  if (setting.data.font_italic.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_FONT_ITALIC_EN_BIT);
    data.font_italic = setting.data.font_italic.value.v_int;
  }
  if (setting.data.bmp.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_BMP_EN_BIT);
    snprintf(data.bmp, OVERLAY_MAX_FILENAME, "%s", setting.data.bmp.str.c_str());
  }
  if (setting.data.color_key.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_BMP_COLOR_EN_BIT);
    data.color_key = setting.data.color_key.value.v_uint;
  }
  if (setting.data.color_range.is_set) {
    SET_BIT(data.enable_bits, AM_OVERLAY_BMP_COLOR_EN_BIT);
    data.color_range = setting.data.color_range.value.v_int;
  }

  g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE,
                            &data, sizeof(data),
                            &service_result, sizeof(service_result));
  if (service_result.ret != 0) {
    ERROR("failed to update data to area!\n");
  }
}

static void set_overlay_state(uint32_t stream_id,
                              const overlay_setting &setting)
{
  if (!setting.is_set) {
    return;
  }
  am_service_result_t service_result = {0};
  am_overlay_id_s param = {0};

  param.stream_id = stream_id;
  if (setting.remove_data.is_set) {
    SET_BIT(param.enable_bits, AM_OVERLAY_DATA_REMOVE_EN_BIT);
    param.area_id = setting.remove_data.area_id;
    param.data_index = setting.remove_data.data_idx;
  } else if (setting.remove.is_set) {
    SET_BIT(param.enable_bits, AM_OVERLAY_REMOVE_EN_BIT);
    param.area_id = setting.remove.area_id;
  } else if (setting.enable.is_set) {
    SET_BIT(param.enable_bits, AM_OVERLAY_ENABLE_EN_BIT);
    param.area_id = setting.enable.area_id;
  } else if (setting.disable.is_set) {
    SET_BIT(param.enable_bits, AM_OVERLAY_DISABLE_EN_BIT);
    param.area_id = setting.disable.area_id;
  } else {
    return;
  }

  g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET,
                            &param, sizeof(param),
                            &service_result, sizeof(service_result));
  if (service_result.ret != 0) {
    ERROR("failed to set overlay!\n");
  }
}

static int32_t set_overlay()
{
  int32_t ret = 0;

  for (auto &m : g_overlay_setting) {
    if (!(m.second.is_set)) {
      continue;
    }

    if (m.second.init.is_set) {
      init_overlay_area(m.first, m.second.init);
    } else if (m.second.add_data.is_set) {
      add_data_to_area(m.first, m.second.add_data);
    } else if (m.second.update_data.is_set) {
      update_area_data(m.first, m.second.update_data);
    } else if (m.second.show.is_set) {
      get_overlay_parameter(m.first, m.second.show);
    } else {
      set_overlay_state(m.first, m.second);
    }
  }
  return ret;
}

static int32_t destroy_overlay()
{
  int32_t ret = 0;
  am_service_result_t service_result;
  g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY, nullptr, 0,
                            &service_result, sizeof(service_result));
  if ((ret = service_result.ret) != 0) {
    ERROR("failed to destroy overlay!\n");
  }
  return ret;
}

static int32_t save_overlay_parameter()
{
  int32_t ret = 0;
  INFO("save overlay configure!!!\n");
  am_service_result_t service_result;
  g_api_helper->method_call(AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE, nullptr, 0,
                            &service_result, sizeof(service_result));
  if ((ret = service_result.ret) != 0) {
    ERROR("failed to destroy overlay!\n");
  }
  return ret;
}

static int32_t show_all_info()
{
  int32_t ret = 0;
  do {
    if ((ret = show_stream_status()) < 0) {
      break;
    }
    if ((ret = show_dptz_ratio()) < 0) {
      break;
    }
    if ((ret = show_dptz_size()) < 0) {
      break;
    }
    if ((ret = show_warp_cfg()) < 0) {
      break;
    }
    if ((ret = show_lbr_cfg()) < 0) {
      break;
    }
    if (show_bitrate_info() < 0) {
      break;
    }
    if (show_framefactor_info() < 0) {
      break;
    }
    if (show_mjpeg_info() < 0) {
      break;
    }
    if (show_h264_gop_info() < 0) {
      break;
    }
    if (show_stream_offset_info() < 0) {
      break;
    }
  } while (0);
  return ret;
}

int32_t main(int32_t argc, char **argv)
{
  if (argc < 2) {
    usage(argc, argv);
    return -1;
  }

  signal(SIGINT, sigstop);
  signal(SIGQUIT, sigstop);
  signal(SIGTERM, sigstop);

  if (init_param(argc, argv) < 0) {
    return -1;
  }

  int32_t ret = 0;
  g_api_helper = AMAPIHelper::get_instance();
  if (!g_api_helper) {
    ERROR("unable to get AMAPIHelper instance\n");
    return -1;
  }

  do {
    if (show_info_flag) {
      show_all_info();
      break;
    } else {
      if (show_status_flag) {
        show_stream_status();
      }
      if (show_warp_flag) {
        show_warp_cfg();
      }
      if (show_dptz_ratio_flag) {
        show_dptz_ratio();
      }
      if (show_dptz_size_flag) {
        show_dptz_size();
      }
      if (show_lbr_flag) {
        show_lbr_cfg();
      }
      if (show_eis_flag) {
        show_eis_cfg();
      }
      if (show_bitrate_flag) {
        show_bitrate_info();
      }
      if (show_framefactor_flag) {
        show_framefactor_info();
      }
      if (show_mjpeg_flag) {
        show_mjpeg_info();
      }
      if (show_h264_gop_flag) {
        show_h264_gop_info();
      }
      if (show_stream_offset_flag) {
        show_stream_offset_info();
      }
    }
    if (get_platform_buffer_num_max_flag) {
      show_buffer_number_max();
      break;
    }
    if (get_platform_stream_num_max_flag) {
      show_stream_number_max();
      break;
    }

    if (restart_flag) {
      if ((ret = restart_encoding()) < 0) {
        break;
      }
    } else if (resume_flag) {
      if ((ret = start_encoding()) < 0) {
        break;
      }
    } else if (pause_flag) {
      if ((ret = stop_encoding()) < 0) {
        break;
      }
    }
    if (set_dptz_ratio_flag) {
      if ((ret = set_dptz_ratio()) < 0) {
        break;
      }
    } else if (set_dptz_size_flag) {
      if ((ret = set_dptz_size()) < 0) {
        break;
      }
    }
    if ((ret = set_warp_config()) < 0) {
      break;
    }
    if ((ret = set_lbr_config()) < 0) {
      break;
    }
    if ((ret = set_eis_config()) < 0) {
      break;
    }
    if ((ret = set_bitrate()) < 0) {
      break;
    }
    if ((ret = set_framefactor()) < 0) {
      break;
    }
    if ((ret = set_mjpeg_quality()) < 0) {
      break;
    }
    if ((ret = set_h264_gop()) < 0) {
      break;
    }
    if ((ret = set_stream_offset()) < 0) {
      break;
    }
    if ((ret = stop_vout()) < 0) {
      break;
    }
    if(force_idr_id) {
      if ((ret = set_force_idr()) < 0) {
        break;
      }
    }

    if (destroy_overlay_flag) {
      if ((ret = destroy_overlay()) < 0) {
        break;
      }
    }
    if (get_overlay_max_flag) {
      show_overlay_max();
    }
    if (save_overlay_flag) {
      if ((ret = save_overlay_parameter()) < 0) {
        break;
      }
    }
    if ((ret = set_overlay()) < 0) {
      break;
    }
  } while (0);

  return ret;
}
