/*******************************************************************************
 * am_encode_warp.cpp
 *
 * History:
 *   Oct 15, 2015 - [zfgong] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents (“Software”) are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

#include "am_base_include.h"
#include "am_define.h"
#include "am_log.h"

#include "am_encode_warp.h"
#include "am_video_utility.h"
#include "am_configure.h"
#include "am_vin.h"

void AMEncodeWarp::destroy()
{
  if (AM_LIKELY(m_platform != nullptr)) {
    m_platform->unmap_warp();
  }
  delete this;
}

bool AMEncodeWarp::start()
{
  return (AM_RESULT_OK == apply());
}

bool AMEncodeWarp::stop()
{
  return true;
}

std::string& AMEncodeWarp::name()
{
  return m_name;
}

AMEncodeWarp::AMEncodeWarp(const char *name) :
  m_vin(nullptr),
  m_platform(nullptr),
  m_buffer_config(nullptr),
  m_ldc_enable(false),
  m_ldc_checked(false),
  m_pixel_width_um(0.0),
  m_name(name)
{
  memset(&m_distortion_lut, 0, sizeof(m_distortion_lut));
}

AMEncodeWarp::~AMEncodeWarp()
{
  m_platform = nullptr;
  m_buffer_config = nullptr;
}

AM_RESULT AMEncodeWarp::init(AMVin *vin)
{
  AM_RESULT result = AM_RESULT_OK;
  do {
    m_vin = vin;
    if (AM_UNLIKELY(!m_vin)) {
      ERROR("Invalid VIN device!");
      result = AM_RESULT_ERR_DSP;
      break;
    }
    if (AM_UNLIKELY(!(m_platform = AMIPlatform::get_instance()))) {
      result = AM_RESULT_ERR_MEM;
      ERROR("Failed to get AMIPlatform!");
      break;
    }
    if (AM_UNLIKELY(!(m_buffer_config = AMBufferConfig::get_instance()))) {
      result = AM_RESULT_ERR_MEM;
      ERROR("Failed to get AMBufferConfig!");
      break;
    }
    if (AM_UNLIKELY(m_platform->map_warp(m_mem)) < 0) {
      ERROR("Failed to map warp!\n");
      result = AM_RESULT_ERR_MEM;
      break;
    }
  } while (0);
  return result;
}

AM_RESULT AMEncodeWarp::set_ldc_strength(float strength)
{
  ERROR("Dummy warp, should be implemented in sub-class!");
  return AM_RESULT_ERR_INVALID;
}

AM_RESULT AMEncodeWarp::get_ldc_strength(float &strength)
{
  ERROR("Dummy warp, should be implemented in sub-class!");
  return AM_RESULT_ERR_INVALID;
}

AM_RESULT AMEncodeWarp::set_pano_hfov_degree(float degree)
{
  ERROR("Dummy warp, should be implemented in sub-class!");
  return AM_RESULT_ERR_INVALID;
}

AM_RESULT AMEncodeWarp::get_pano_hfov_degree(float &degree)
{
  ERROR("Dummy warp, should be implemented in sub-class!");
  return AM_RESULT_ERR_INVALID;
}

AM_RESULT AMEncodeWarp::set_warp_region_yaw(int yaw)
{
  ERROR("Dummy warp, should be implemented in sub-class!");
  return AM_RESULT_ERR_INVALID;
}

AM_RESULT AMEncodeWarp::get_warp_region_yaw(int &yaw)
{
  ERROR("Dummy warp, should be implemented in sub-class!");
  return AM_RESULT_ERR_INVALID;
}


AM_RESULT AMEncodeWarp::apply()
{
  ERROR("Dummy warp, should be implemented in sub-class!");
  return AM_RESULT_ERR_INVALID;
}

AM_RESULT AMEncodeWarp::load_config()
{
  ERROR("Dummy warp, should be implemented in sub-class!");
  return AM_RESULT_ERR_INVALID;
}

AM_RESULT AMEncodeWarp::save_config()
{
  ERROR("Dummy warp, should be implemented in sub-class!");
  return AM_RESULT_ERR_INVALID;
}

bool AMEncodeWarp::is_ldc_enable()
{
  do {
    if (!m_ldc_checked) {
      m_ldc_checked = true;
      if (AM_UNLIKELY(m_platform->check_ldc_enable(m_ldc_enable))) {
        ERROR("Failed to check_ldc_enable!\n");
        break;
      }
    }
  } while (0);
  return m_ldc_enable;
}
